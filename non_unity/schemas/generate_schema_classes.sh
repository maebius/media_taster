#!/bin/bash
MONO_PREFIX=/Library/Frameworks/Mono.framework/Versions/2.10.11
MONO=$MONO_PREFIX/bin/mono
XSD=$MONO_PREFIX/lib/mono/2.0/xsd.exe

export MONO_PATH=$MONO_PREFIX/lib/mono/2.0
export MONO_CFG_DIR=$MONO_PREFIX/etc
export LD_LIBRARY_PATH=$MONO_PREFIX/lib

INPUT_DIR="."
OUTPUT_CS_PATH=../../Assets/Scripts/Model
OUTPUT_XML_PATH=../../Assets/Xml
NAMESPACE=MediaTaster.Model

INPUT_FILE_NO_POSTFIX="MediaTasterSchema"

echo "Input dir: " $INPUT_DIR
echo "Output dir: " $OUTPUT_CS_PATH

eval $MONO $XSD $INPUT_DIR/$INPUT_FILE_NO_POSTFIX.xsd /namespace:$NAMESPACE /c /o:$OUTPUT_CS_PATH
eval cp $INPUT_DIR/$INPUT_FILE_NO_POSTFIX.xsd $OUTPUT_XML_PATH/$INPUT_FILE_NO_POSTFIX.xml
