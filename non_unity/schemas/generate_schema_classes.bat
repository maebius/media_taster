@echo off

cls

set XSD_PATH="C:\Program Files (x86)\Microsoft SDKs\Windows\v8.0A\bin\NETFX 4.0 Tools\xsd.exe"
set OUTPUT_CS_PATH="..\\..\\Assets\\Scripts\\Model"
set OUTPUT_XML_PATH="..\\..\\Assets\\Xml"
set NAMESPACE="MediaTaster.Model"
set INPUT_FILE_NO_POSTFIX="MediaTasterSchema"

%XSD_PATH% /n:%NAMESPACE% /c /o:%OUTPUT_CS_PATH% %INPUT_FILE_NO_POSTFIX%.xsd
copy %INPUT_FILE_NO_POSTFIX%.xsd %OUTPUT_XML_PATH%\\%INPUT_FILE_NO_POSTFIX%.xml

