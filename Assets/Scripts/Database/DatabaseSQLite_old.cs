﻿namespace weMarchive.data
{
	using UnityEngine;
	using System;
	using System.Data;
	using System.Collections.Generic;
	using Mono.Data.SqliteClient;
	
	using DataTable = System.Data.DataTable;

	public class DatabaseSqlite
	{
		/// <summary>
		/// Object to handle sql connections.
		/// </summary>

		public List<string> ActiveTableNames
		{
			get { return m_activeTableNames; }
		}
		
		public string GetSourceId()
		{
			return SourceId;
		}
		public void SetSourceId(string id)
		{
			SourceId = id;
		}

		private string m_sourceId = null;
		public string SourceId
		{
			set
			{
				m_sourceId = value;
			}
			get
			{
				return m_sourceId;
			}
		}

		public DatabaseSqlite(string filePath)
		{
			m_filePath = filePath;
		}

		public string GetId()
		{
			return m_filePath;
		}

		public void Close()
		{
			if (m_dbConnection != null)
			{
				m_dbConnection.Close();
			}
		}

		public void Connect(string databaseName)
		{
			Close();

			string connStr = String.Format("URI=file:{0}", m_filePath);

			try
			{
				m_dbConnection = (IDbConnection) new SqliteConnection(connStr);
				m_dbConnection.Open();

				//Debug.Log(string.Format("DatabaseSQLite.Connect() - state: {0} ", m_dbConnection.State));
			}
			catch (Exception ex)
			{
				Debug.LogError(string.Format("Error connecting to the server: {0} ", ex.Message));
			}

			SetDatabase(databaseName);
		}


		public Dictionary<object, Dictionary<string, object>> DoQuery(string query)
		{
			IDbCommand cmd = m_dbConnection.CreateCommand();
			cmd.CommandText = query;
			IDataReader reader = (IDataReader) cmd.ExecuteReader();

			Dictionary<object, Dictionary<string, object>> result = new Dictionary<object, Dictionary<string, object>>();
			
			try
			{
				reader = cmd.ExecuteReader();

				while (reader.Read())
				{
					Dictionary<string, object> subResult = new Dictionary<string, object>();

					object key = reader.GetValue(0);

					for (int i = 0; i < reader.FieldCount; i++)
					{
						object o = reader.GetValue(i);
						subResult.Add(reader.GetName(i), o);
					}
					result[key] = subResult;
				}
			}
			catch (Exception ex)
			{
				Debug.LogError(string.Format("Failed to populate database list: {0} ", ex.Message));
			}
			finally
			{
				if (reader != null) reader.Close();
				reader = null;
				cmd.Dispose();
				cmd = null;
			}
			return result;
		}

		public void SetDatabase(string name)
		{
			IDataReader reader = null;
			IDbCommand cmd = m_dbConnection.CreateCommand();
			cmd.CommandText = string.Format(
						"SELECT name FROM {0}.Sqlite_master WHERE type='table'", name);
			
			try
			{
				reader = cmd.ExecuteReader();

				m_activeTableNames.Clear();
				while (reader.Read())
				{
					m_activeTableNames.Add(reader.GetString(0));
				}
			}
			catch (Exception ex)
			{
				Debug.LogError(string.Format("Failed to populate table list: {0} ", ex.Message));
			}
			finally
			{
				if (reader != null) reader.Close();
				reader = null;
				cmd.Dispose();
				cmd = null;
			}
		}

		public DataTable GetTable(string name)
		{
			if (m_dataTables.ContainsKey(name))
			{
				return m_dataTables[name];
			}
			return null;
		}

		public DataTable RefreshTable(string name)
		{
			if (m_dataTables.ContainsKey(name) && m_sqlDataAdapters.ContainsKey(name))
			{
				m_dataTables.Remove(name);
				m_sqlDataAdapters.Remove(name);
				InitializeTable(name);
				return m_dataTables[name];
			}
			return null;
		}

		// handler for RowUpdating event, used to debug/print
		private static void OnRowUpdating(object sender, System.Data.DataRowChangeEventArgs e)
		
		{
			string rowItemString = String.Format("state: {0}", e.Row.RowState.ToString());
			if (e.Row.RowState != DataRowState.Deleted)
			{
				rowItemString += String.Format(", values -");
				for (int i = 0; i < e.Row.ItemArray.Length; i++)
				{
					rowItemString += String.Format(" {0} : \"{1}\"", i, e.Row.ItemArray[i].ToString());
				}
			}
			//Console.WriteLine("Row Changed - action: {0}, Row: {1}", e.Action, rowItemString);
		}

		public void InitializeTable(string name)
		{
			if (!m_sqlDataAdapters.ContainsKey(name))
			{
				SqliteDataAdapter adapter =
					new SqliteDataAdapter("SELECT * FROM " + name, (SqliteConnection) m_dbConnection);

				adapter.AcceptChangesDuringFill = true;
				adapter.AcceptChangesDuringUpdate = true;

				DataTable table = new DataTable();

				m_dataTables.Add(name, table);
				m_sqlDataAdapters.Add(name, adapter);
			}

			m_dataTables[name].Clear();
			m_sqlDataAdapters[name].Fill(m_dataTables[name]);
		}

		public void SetRowChangignListener(string tableName)
		{
			DataTable table = m_dataTables[tableName];
			table.RowChanging += new DataRowChangeEventHandler(OnRowUpdating);
		}

		public void PrintTableToCommandLine(DataTable data)
		{
			Console.WriteLine(
				String.Format("Table: {0}, column count: {1}, row count: {2}",
					data.TableName, data.Columns.Count, data.Rows.Count));

			foreach (DataColumn col in data.Columns)
			{
				Console.Write(col.ColumnName + "\t");
			}
			Console.WriteLine("\n\n");

			int itemIndex = 0;
			foreach (DataRow row in data.Rows)
			{
				Console.Write(String.Format("{0} : ", itemIndex++));
				foreach (System.Object item in row.ItemArray)
				{
					Console.Write(String.Format("{0} ", item.ToString()));
				}
				Console.WriteLine("");
			}
		}

		public void CopyData(string tableName, out DataTable data)
		{
			if (!m_dataTables.ContainsKey(tableName))
			{
				data = null;
				return;
			}

			data = m_dataTables[tableName].Copy();
		}

		public void UpdateChangedValues(string tableName, DataTable changedData)
		{
			if (!m_dataTables.ContainsKey(tableName) || changedData == null)
			{
				return;
			}

			DataTable data = m_dataTables[tableName];
			data.AcceptChanges();
			m_sqlDataAdapters[tableName].Fill(changedData);
			m_sqlDataAdapters[tableName].Update(changedData);

		}

		public void ExportTableToXml(string tableName, string fileName)
		{
			if (!m_dataTables.ContainsKey(tableName))
			{
				return;
			}
			m_dataTables[tableName].WriteXml(fileName, XmlWriteMode.WriteSchema, true);
		}

		private IDbConnection m_dbConnection;
		
		/// <summary>
		/// Path to our database file.
		/// </summary>
		private string m_filePath;
		
		/// <summary>
		/// The data of the table. If this is synced with sqlDataAdapter,
		/// then the source data in the mysql table is altered also.
		/// </summary>
		private Dictionary<string, DataTable> m_dataTables =
			new Dictionary<string, DataTable>();
		
		/// <summary>
		/// Object to convert sql data from mysql to our data objects.
		/// </summary>
		private Dictionary<string, SqliteDataAdapter> m_sqlDataAdapters =
			new Dictionary<string, SqliteDataAdapter>();
		
		private List<string> m_activeTableNames = new List<string>();
	}
}
