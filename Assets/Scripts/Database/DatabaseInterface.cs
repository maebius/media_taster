﻿namespace Database
{
	using System.Collections.Generic;

	public interface DatabaseInterface 
	{
		DataTable GetTable(string name);

		List<string> GetTableNames();

		void SaveChanges();

		DataTable Query(string query);
	}
}

