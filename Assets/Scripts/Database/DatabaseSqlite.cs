﻿namespace Database
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using UnityEngine;
	using System.Data;
	using Mono.Data.SqliteClient;

	using DataTable = System.Data.DataTable;

	public class DatabaseSqlite : DatabaseInterface
	{

		#region Properties
		public virtual List<string> GetTableNames()
		{
			return m_tableNames;
		}
		#endregion

		#region Constructors
		public DatabaseSqlite(string filePath, string databaseName)
		{
			m_filePath = filePath;
			m_databaseName = databaseName;

			string connStr = String.Format("URI=file:{0}", m_filePath);

			try
			{
				Debug.Log(string.Format("connection string: {0}", connStr));

				m_dbConnection = (IDbConnection)new SqliteConnection(connStr);

				Debug.Log(string.Format("DatabaseSQLite.Connect() - state before open: {0} ", m_dbConnection.State));

				m_dbConnection.Open();

				Debug.Log(string.Format("DatabaseSQLite.Connect() - state after open: {0} ", m_dbConnection.State));
			}
			catch (Exception ex)
			{
				Debug.LogError(string.Format("Error connecting to the server: {0} ", ex.Message));
			}

			UpdateTableNames();
		}
		#endregion

		#region Interface implementation - DatabaseInterface
		public void SaveChanges()
		{
			throw new System.NotImplementedException();
		}

		public void InitializeTable(string name)
		{
			if (!m_sqlDataAdapters.ContainsKey(name))
			{
				SqliteDataAdapter adapter =
					new SqliteDataAdapter("SELECT * FROM " + name, (SqliteConnection)m_dbConnection);

				adapter.AcceptChangesDuringFill = true;
				adapter.AcceptChangesDuringUpdate = true;

				System.Data.DataTable table = new System.Data.DataTable();

				m_dataTables.Add(name, table);
				m_sqlDataAdapters.Add(name, adapter);
			}

			m_dataTables[name].Clear();
			m_sqlDataAdapters[name].Fill(m_dataTables[name]);
		}

		public Database.DataTable GetTable(string name)
		{
//			if (m_dataTables.ContainsKey(name))
//			{
//				return m_dataTables[name];
//			}
			return null;
		}

		public Database.DataTable Query(string query)
		{
			return null;
		}

		public Dictionary<object, Dictionary<string, object>> Query_OLD(string query)
		{
			IDbCommand cmd = m_dbConnection.CreateCommand();
			cmd.CommandText = query;
			IDataReader reader = (IDataReader)cmd.ExecuteReader();

			Dictionary<object, Dictionary<string, object>> result = new Dictionary<object, Dictionary<string, object>>();

			try
			{
				reader = cmd.ExecuteReader();

				while (reader.Read())
				{
					Dictionary<string, object> subResult = new Dictionary<string, object>();

					object key = reader.GetValue(0);

					for (int i = 0; i < reader.FieldCount; i++)
					{
						object o = reader.GetValue(i);
						subResult.Add(reader.GetName(i), o);
					}
					result[key] = subResult;
				}
			}
			catch (Exception ex)
			{
				Debug.LogError(string.Format("Failed to populate database list: {0} ", ex.Message));
			}
			finally
			{
				if (reader != null) reader.Close();
				reader = null;
				cmd.Dispose();
				cmd = null;
			}
			return result;
		}
		#endregion



		#region Private methods
		private void UpdateTableNames()
		{
			IDataReader reader = null;
			IDbCommand cmd = m_dbConnection.CreateCommand();
			cmd.CommandText = string.Format(
						"SELECT name FROM {0}.Sqlite_master WHERE type='table'", m_databaseName);

			try
			{
				reader = cmd.ExecuteReader();

				m_tableNames.Clear();
				while (reader.Read())
				{
					m_tableNames.Add(reader.GetString(0));
				}
			}
			catch (Exception ex)
			{
				Debug.LogError(string.Format("Failed to populate table list: {0} ", ex.Message));
			}
			finally
			{
				if (reader != null) reader.Close();
				reader = null;
				cmd.Dispose();
				cmd = null;
			}
		}
		#endregion

		#region Members, private
		/// <summary>
		/// Object to handle sql connections.
		/// </summary>
		private IDbConnection m_dbConnection;
		
		/// <summary>
		/// Path to our database file.
		/// </summary>
		private string m_filePath;
		
		/// <summary>
		/// Name of the database.
		/// </summary>
		private string m_databaseName;
		
		/// <summary>
		/// All the table names found in the database.
		/// </summary>
		private List<string> m_tableNames = new List<string>();
		
		/// <summary>
		/// The data of the table. If this is synced with sqlDataAdapter,
		/// then the source data in the sql table is altered also.
		/// </summary>
		private Dictionary<string, System.Data.DataTable> m_dataTables =
			new Dictionary<string, System.Data.DataTable>();
		
		/// <summary>
		/// Object to convert sql data from mysql to our data objects.
		/// </summary>
		private Dictionary<string, SqliteDataAdapter> m_sqlDataAdapters =
			new Dictionary<string, SqliteDataAdapter>();
		#endregion
	}
}

