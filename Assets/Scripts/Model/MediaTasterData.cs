
namespace MediaTaster.Model
{
	using System;
	using System.Collections;
	using System.Collections.Generic;
	using System.ComponentModel;
	//using System.Data;
	using System.Diagnostics;
	using System.Reflection;
	using System.Xml;
	using System.Xml.Schema;
	using System.Xml.Serialization;
	using System.IO;
	using System.Globalization;
	using System.Threading;

	//using Maebius.Utility;
	
	using System.Runtime.Serialization.Formatters.Binary;
	using System.Runtime.Serialization;

	using Database;
	using SqliteDatabase;

	[Serializable]
	public class MediaTasterData
	{
		#region Properties
		/// <summary>
		/// Getter for our singleton object of this class.
		/// </summary>
		/// <returns></returns>
		public static MediaTasterData Instance { get; private set; }
	
		public DatabaseInterface Database { get; set; }
		public List<string> TableNames { get; private set; }
		public bool TableChanged { get; private set; }

		public string SelectedTableName { get; private set; }
		public CultureInfo CultureInfo { get; private set; }
		public bool ForceUpdate { get; private set; }
		public bool ShowOnlyAvailable { get; private set; }
		public bool AllowReinitOfDefaultSearch { get; set; }

		public xsd_MediaTaster Properties 
		{ 
			get { return m_properties; } 
			set { m_properties = value; }
		}
	
		public xsd_ct_Column ClickedColumn 
		{ 
			get { return m_clickedColumn; } 
			set { m_clickedColumn = value; }
		}

	
		#endregion

		#region Methods
		public MediaTasterData(byte[] configBytes, string leadingPath = null)
		{
			Instance = this;

			TableNames = new List<string>();

			XmlReader reader = XmlTools.CreateReader(configBytes, null, typeof(xsd_MediaTaster));
			if (reader != null)
			{
				Properties = (xsd_MediaTaster)XmlTools.Serializer.Deserialize(reader);
				XmlTools.CleanUpSerializer();
			}
			else
			{
				throw new Exception(string.Format("Epic fail! Could not load default save data, so should bail out."));
			}
			Initialize(leadingPath);
		}

		public void SaveAsBinaryToFile(string filePath)
		{
			using (FileStream stream = new FileStream(filePath, FileMode.Create))
			{
				IFormatter formatter = new BinaryFormatter();
				formatter.Serialize(stream, this);
			}
		}

		/// <summary>
		/// Gets the corresponding database object for identifier, or null if there is none.
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public DatabaseInterface GetDatabase(string id)
		{
			Database = m_databases.ContainsKey(id) ? m_databases[id] : null;
			return Database;
		}

	
		public int GetQueryCount(string key)
		{
			if (m_queries.ContainsKey(key))
			{
				return m_queries[key].Rows.Count;
			}
			return 0;
		}

		public object GetQuery(string key, string name, string indexColumn)
		{
//			if (m_queries.ContainsKey(key))
//			{
//				if (m_queries[key].Columns.Contains(indexColumn))
//				{
//					if (m_queries[key][indexColumn].ContainsKey(name))
//					{
//						return m_queries[key][indexColumn][name];
//					}
//				}
//			}
			return null;
		}

		public void ProcessTableDirectorySources(string[] dirs)
		{
			// check if the given dirs are found in found hard disks,
			// and add found combinations as runtime parameters
			/*
			foreach (string dir in dirs)
			{
				foreach (DriveEntry drive in m_driveInfo)
				{
					string path = System.IO.Path.Combine(drive.Letter, dir);
					path = System.IO.Path.GetFullPath(path);
					if (System.IO.Directory.Exists(path))
					{
						// is there a config file?
						string configFile = System.IO.Path.Combine(path, "wemarchive_folder.xml");
						if (!System.IO.File.Exists(configFile))
							continue;

						try
						{
							XmlReader xmlReader = XmlReader.Create(configFile);
							while (xmlReader.Read())
							{
								if (xmlReader.Name.Equals("Folder") 
									&& (xmlReader.NodeType == XmlNodeType.Element))
								{
									string id = xmlReader.GetAttribute("Id");
									
									// add a mapping
									m_runTimeParameters["folder"].Add(
										id, path);
								}
							}
						}
						catch (System.Exception e)
						{
							string message =
								"With xml:\t\"" + configFile +
								"\"\n\nerror in " + Classes.GetTypeName() + "." + Classes.GetMethodName() + ":\n\n" +
								Classes.GetExceptionDump(e);

							MessageBox.Show(message,
							  "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
						}
			
					}
				}
			}*/

		}

		private void Initialize(string leadingPath = null)
		{
			//m_cultureInfo = CultureInfo.InvariantCulture;
			CultureInfo = new CultureInfo("fi-FI");
			CultureInfo.NumberFormat.NumberDecimalSeparator = ",";

			// add all disc drives as parameters, in form "disc id" -> "disc letter"
			/*
			m_driveInfo = Browser.GetDriveInfo();

			m_runTimeParameters.Add("disc", new Dictionary<string, string>());
			foreach (DriveEntry drive in m_driveInfo)
			{
				if (!string.IsNullOrEmpty(drive.Serial))
					m_runTimeParameters["disc"].Add(drive.Serial, drive.Letter);
			}*/

			// also init for folders ..
			m_runTimeParameters.Add("folder", new Dictionary<string, string>());

			/*
			if (m_properties.DatabaseSource != null)
			{
				foreach (xsd_ct_Database databaseData in m_properties.DatabaseSource)
				{
					if (databaseData != null)
					{
						DatabaseMySql database = new DatabaseMySql(databaseData);
						m_databases.Add(databaseData.Id, database);
					}
				}
			}*/


			if (Properties.FileSource != null)
			{
				foreach (xsd_ct_File fileData in Properties.FileSource)
				{
					if (fileData != null)
					{
						string filePath = fileData.Filename;
						if (!string.IsNullOrEmpty(leadingPath))
						{
							filePath = Path.Combine (leadingPath, fileData.Filename);
						}

						//DatabaseInterface database = new DatabaseSqlite(filePath, "main");
						DatabaseInterface database = new SqliteDatabase(filePath);

						m_databases.Add(fileData.Id, database);
					}
				}
			}
			if (Properties.Table != null)
			{
				foreach (xsd_ct_Table tableData in Properties.Table)
				{
					if (tableData != null)
					{
						Table table = new Table(tableData);
						m_tables.Add(tableData.Name, table);
						TableNames.Add(tableData.Name);
					}
				}
			}

			// initialize our ActivityFeeds (these are some "direct sql lookups" that act as sneak peak info for data
			if (Properties.ActivityFeed != null)
			{
				foreach (xsd_ct_ActivityFeed af in Properties.ActivityFeed)
				{
					if (!m_databases.ContainsKey(af.Database))
					{
						string message = String.Format("Did not find database named \"{0}\"!", af.Database);
						throw new Exception(message);
					}

					// first, do all the queries for database
					DatabaseInterface db = m_databases[af.Database];
					foreach (xsd_ct_Query q in af.Query)
					{
						m_queries[q.Name] = db.Query(q.SQL);
					}

					// ... then initiate the Tables that supposedly use queries
					if (af.Table != null)
					{
						foreach (xsd_ct_Table tableData in af.Table)
						{
							if (tableData != null)
							{
								Table table = new Table(tableData);
								m_tables.Add(tableData.Name, table);
								TableNames.Add(tableData.Name);
							}
						}
					}
				}
			}

			if (Properties.HarddiscMapping != null)
			{
				// initialize our mappings
				foreach (xsd_ct_Mapping mapping in Properties.HarddiscMapping)
				{
					m_columnValueMappings.Add(mapping.Source, mapping.Target);
				}
			}

			// do some pre-loading for all tables
			foreach (Table table in m_tables.Values)
			{
				table.Initialize();
			}

			OrderTableUpdate();
		}

		public string GetMapping(object key)
		{
			string keyAsString = key.ToString();
			if (m_columnValueMappings.ContainsKey(keyAsString))
				return m_columnValueMappings[keyAsString];

			return null;
		}

		public string GetRunTimeParameter(string category, string key)
		{
			if (string.IsNullOrEmpty(key))
				return null;

			if (m_runTimeParameters.ContainsKey(category))
			{
				Dictionary<string, string> values = m_runTimeParameters[category];
				if (values.ContainsKey(key))
					return values[key];

				return "";
			}
			return "";
		}

		public void UpdateActivityFeeds(string databaseName)
		{
			// initialize our ActivityFeeds (these are some "direct sql lookups" that act as sneak peak info for data
			foreach (xsd_ct_ActivityFeed af in Properties.ActivityFeed)
			{
				if (!m_databases.ContainsKey(af.Database))
				{
					string message = String.Format("Did not find database named \"{0}\"!", af.Database);
					throw new Exception(message);
				}

				if (af.Database != databaseName)
					continue;

				// first, do all the queries for database
				DatabaseInterface db = m_databases[af.Database];
				foreach (xsd_ct_Query q in af.Query)
				{
					m_queries[q.Name] = db.Query(q.SQL);
				}
			}
		}

		public void AddChangedTable(string tableName)
		{
			if (!m_tablesChanged.Contains(tableName))
			{
				m_tablesChanged.Add(tableName);
			}

			//MainForm.Instance.RefreshLoadingStatus();
		}


		public void CheckChangedTables()
		{
			int index = 0;
			foreach (string tableName in m_tablesChanged)
			{
				// The object who called was a table (meaning it finished loading) and we have selected it?
				if (!String.IsNullOrEmpty(tableName) && tableName.Equals(SelectedTableName))
				{
					// ... then need to update its visuals
					// ... (unless it is still dirty ...)
					if (!GetTable(tableName).Dirty)
					{
						//MainForm.Instance.RebuildCurrentTableVisualisation();
						m_tablesChanged.RemoveAt(index);
						return;
					}
				}
				index++;
			}
			// also, we might have force-updated ... (case when doing search-filtering)
			if (ForceUpdate)
			{
				ForceUpdate = false;
				//MainForm.Instance.RebuildCurrentTableVisualisation();
			}
		}

		public void RefreshThreadedTableUpdateStatus()
		{
			// Wait until it is safe to enter.
			m_mutex.WaitOne();

			foreach (Table t in m_tableInitOrder)
			{
				if (t.ReadyToBeUpdated)
				{
					t.MarkedForUpdate = true;
					ThreadPool.QueueUserWorkItem(t.InitThreadPoolCallback, m_updateThreadIndex++);
				}
			}
			// Release the Mutex.
			m_mutex.ReleaseMutex();
		}

		public void OrderTableUpdate()
		{
			// init whit no-dependencies tables
			foreach (Table t in m_tables.Values)
			{
				if (t.DependenciesOneLevel.Count == 0)
				{
					m_tableInitOrder.Add(t);
				}
			}
			// then, round by round (dependency-depth-level), 
			// add every other table that has all it's dependecies already added
			while (m_tableInitOrder.Count < m_tables.Count)
			{
				foreach (Table t in m_tables.Values)
				{
					if (!m_tableInitOrder.Contains(t))
					{
						bool add = true;
						foreach (KeyValuePair<Table, xsd_ct_Column> depencency in t.DependenciesRecursive)
						{
							// if has a depdency that has not been added, should not be added yet
							if (!m_tableInitOrder.Contains(depencency.Key))
							{
								add = false;
								break;
							}
						}
						if (add)
						{
							m_tableInitOrder.Add(t);
						}
					}
				}
			}
		}

		public void RefreshTable(string tableName)
		{
			if (!m_tables.ContainsKey(tableName))
			{
				return;
			}

			Table table = m_tables[tableName];
			table.FullUpdate();
		}

		public void RefreshAllTables()
		{
			foreach (string name in m_tables.Keys)
			{
				RefreshTable(name);
			}
		}

		public void SetTable(string name)
		{
			TableChanged = name != SelectedTableName;

			SelectedTableName = name;
			if (!m_tables.ContainsKey(SelectedTableName))
			{
				return;
			}

			Table table = m_tables[SelectedTableName];

			if (table.Properties.DoInitialLoading)
			{
				// initial update of data
				table.Update(
					table.CachedSearch,
					table.CachedPartialHitEnabled,
					table.Properties.DefaultColumnForIndex,
					table.CachedStartingStringFilter,
					table.CachedColumn,
					true);
			}
		}

		public Table GetTable(string name)
		{
			if (!String.IsNullOrEmpty(name) && m_tables.ContainsKey(name))
			{
				return m_tables[name];
			}
			return null;
		}

		public void PerformSearch(Dictionary<int, string> search, bool partialMatch, bool updateVisual)
		{
			Table table = GetTable(SelectedTableName);
			table.PageIndex = 0;
			table.Update(search, partialMatch, table.Properties.DefaultColumnForIndex,
			  Table.ALL_ENTRIES_FILTER_ID, table.CachedColumn, true);

			if (updateVisual)
			{
				ForceUpdate = true;
				//MainForm.Instance.ResetCamera = true;
				//MainForm.Instance.Invalidate();
			}
		}

		/// <summary>
		/// Performs a search to current tables cell values. Currently checks what
		/// tables columns are enabled in search, and just does a simple search of the same
		/// key value to all enabled columns.
		/// 
		/// @todo: TODO: make possible to have different search values for different columns
		/// (this is already possible for Table's part, do the interfacing here.)
		/// </summary>
		/// <param name="text"></param>
		public void PerformSearch(string text)
		{
			Table table = GetTable(SelectedTableName);

			// construct search according to what columns are enabled
			Dictionary<int, string> search = new Dictionary<int, string>();

			// this appends to previous search, so fill the possible prev values
			if (table.CachedSearch != null)
			{
				foreach (KeyValuePair<int, string> pair in table.CachedSearch)
				{
					search.Add(pair.Key, pair.Value);
				}
			}

			foreach (xsd_ct_Column col in table.Columns)
			{
				if (col.IncludeInSearch)
				{
					search.Add(col.Index, text);
				}
			}
			if (search.Count == 0)
			{
				return;
			}

			table.Update(search, true, table.Properties.DefaultColumnForIndex,
			  Table.ALL_ENTRIES_FILTER_ID, table.CachedColumn, false);

			ForceUpdate = true;
			//MainForm.Instance.Invalidate();
		}

		public void ResetChanges()
		{
			if (SelectedTableName != null)
			{
				Table table = GetTable(SelectedTableName);
				table.ResetChanges();
			}
		}

		public void SaveChanges()
		{
			if (SelectedTableName != null)
			{
				Table table = GetTable(SelectedTableName);
				table.SaveChanges();

				ForceUpdate = true;
				//MainForm.Instance.RefreshLoadingStatus();
			}
		}

		#endregion


		#region Members Not to be serializabled.

		// NOTE! Because of stupid Unity behavior, we need to mark even private members with NonSerialized attribute

		[NonSerialized]
		private xsd_MediaTaster m_properties; 
		[NonSerialized]
		private xsd_ct_Column m_clickedColumn; 
		[NonSerialized]
		private Dictionary<string, Dictionary<string, string>> m_runTimeParameters = 
			new Dictionary<string, Dictionary<string, string>>();
		[NonSerialized]
		private Dictionary<string, Table> m_tables = new Dictionary<string, Table>();
		[NonSerialized]
		private List<Table> m_tableInitOrder = new List<Table>();
		[NonSerialized]
		private Dictionary<string, string> m_files =
			new Dictionary<string, string>();
		[NonSerialized]
		private int m_updateThreadIndex = 0;
		[NonSerialized]
		private List<string> m_tablesChanged = new List<string>();
		[NonSerialized]
		private Dictionary<string, DatabaseInterface> m_databases =
			new Dictionary<string, DatabaseInterface>();
		[NonSerialized]
		private Mutex m_mutex = new Mutex();
		[NonSerialized]
		private Dictionary<string, string> m_columnValueMappings = new Dictionary<string, string>();
		private Dictionary<string, DataTable> m_queries = new Dictionary<string, DataTable>();

		//private List<DriveEntry> m_driveInfo;
		#endregion
	}
}
