
namespace MediaTaster.Model
{
	using System;
	using System.Collections;
	using System.Collections.Generic;
	using System.Text;
	//using System.Data;
	using System.Diagnostics;
	using System.Xml;
	using System.Xml.Schema;
	using System.IO;
	using System.Xml.Serialization;
	using System.Threading;

	using Database;

	//using Maebius.Utility;

	//using weMarchive.src.forms;

	using System.Runtime.Serialization.Formatters.Binary;
	using System.Runtime.Serialization;
	using MediaTaster.Model;

	// Serialize this whole class by default.
	[Serializable]
	public class Table
	{
		#region Constants
		// Constant members

		/// <summary>
		/// String that is used as a clickable link in index list for entries that are empty.
		/// </summary>
		[NonSerialized]
		public static string INDEX_EMPTY_ENTRY_LETTER_LINK = "-";

		/// <summary>
		/// Name for the category column's name. It is fixed, as the real column name
		/// will change all the time and is not used, so we can use one common for all.
		/// </summary>
		[NonSerialized]
		public static string UNIQUE_NAME_ID = "unique_name";
		/// <summary>
		/// Name for the category column's entry count. It is fixed.
		/// </summary>
		[NonSerialized]
		public static string COUNT_ID = "count";

		/// <summary>
		/// String id for passing all entries through (so, not doing any filtering).
		/// </summary>
		static public String ALL_ENTRIES_FILTER_ID = "all";

		#endregion

		#region Delegates And Events
		// A delegate type for hooking up change notifications.
		public delegate void TableChangedEventHandler(object sender, TableEventArgs e);

		// An event that clients can use to be notified whenever the
		// elements of the list change.
		public event TableChangedEventHandler Changed;

		#endregion

		#region Members Serializabled

		private xsd_ct_Table m_properties;

		/// <summary>
		/// All the rows of the table, that also contain all
		/// the data (the row elements) that the table contains. Used to 
		/// show table values if no filtering enabled, and to build index in every case.
		/// </summary>
		[XmlIgnore]
		private List<TableRowEntry> m_allRows = new List<TableRowEntry>();

		/// <summary>
		/// All the rows of the table, that also contain all
		/// the data (the row elements) that the table contains. Used to 
		/// show table values if no filtering enabled, and to build index in every case.
		/// </summary>
		[XmlIgnore]
		[NonSerialized]
		private List<TableRowEntry> m_visibleRows = new List<TableRowEntry>();

		/// <summary>
		/// The starting letters for tables entries. Used to print 
		/// indices for categories, "these entries start with A" etc.
		/// </summary>
		[XmlIgnore]
		[NonSerialized]
		private SortedList<string, string> m_entryStartingLetters = new SortedList<string, string>();

		[NonSerialized]
		private xsd_ct_Column m_sortByColumn = null;

		[XmlIgnore]
		/// <summary>
		/// The rows of the table, that have passed additional filtering. Actually used
		/// to display table values.
		/// </summary>
		[NonSerialized]
		private List<TableRowEntry> m_searchFilteredRows = new List<TableRowEntry>();

		/// <summary>
		/// All the columns descriptions/names of the table, such as
		/// "name", "id", "publishing year", etc.
		/// </summary>
		[XmlIgnore]
		[NonSerialized]
		private SerializableDictionary<string, xsd_ct_Column> m_categoryColumns =
			new SerializableDictionary<string, xsd_ct_Column>();
		[XmlIgnore]
		[NonSerialized]
		private List<xsd_ct_Column> m_categoryColumnsList = new List<xsd_ct_Column>();

		/// <summary>
		/// Column values that match from our source to the definition in this table.
		/// </summary>
		[NonSerialized]
		private Dictionary<xsd_ct_Column, int> m_indexToColumn = new Dictionary<xsd_ct_Column, int>();

		[NonSerialized]
		private List<Table> m_updatedTables = new List<Table>();

		[NonSerialized]
		private List<KeyValuePair<Table, xsd_ct_Column>> m_oneLevelDependencyTables = new List<KeyValuePair<Table, xsd_ct_Column>>();

		[NonSerialized]
		private List<KeyValuePair<Table, xsd_ct_Column>> m_oneLevelDependentTables = new List<KeyValuePair<Table, xsd_ct_Column>>();

		[NonSerialized]
		private List<KeyValuePair<Table, xsd_ct_Column>> m_recursiveDependencyTables = new List<KeyValuePair<Table, xsd_ct_Column>>();

		[NonSerialized]
		private Dictionary<TableRowEntry, Dictionary<xsd_ct_Column, object>> m_combinedCalculationResult =
		  new Dictionary<TableRowEntry, Dictionary<xsd_ct_Column, object>>();

		#endregion

		#region Members Not want to serialize

		/// <summary>
		/// Dynamically constructed visibility groups for row entries. This needs the table setup in xml so
		/// that we have marked some column as grouping column.
		/// </summary>
		[NonSerialized]
		private Dictionary<string, bool> m_groupVisibilityMemory = new Dictionary<string, bool>();
		/// <summary>
		/// Helper structure for the above, indicating what groups have been already added in iteration so
		/// that we do not end up adding duplicates.
		/// </summary>
		[NonSerialized]
		private List<string> m_groupsAdded = new List<string>();

		[NonSerialized]
		private bool m_sortOrderIncreasing = true;

		[NonSerialized]
		private DataTable m_rawData = null;

		/// <summary>
		/// The database that the table is currently connected to.
		/// If saves are made and this is not null, the updates will go to this
		/// database.
		/// </summary>
		[NonSerialized]
		private DatabaseInterface m_sourceDatabase = null;

		/// <summary>
		/// Our schema, defining the format of the data.
		/// </summary>
		[NonSerialized]
		private XmlSchema m_schema = null;

		[NonSerialized]
		private List<DataRow> m_restrictionFilteredRows = new List<DataRow>();

		/// <summary>
		/// The current page index where we are in the table. The table is paged, so that
		/// only certain amount of entries are shown in one time, and the user can navigate the
		/// pages.
		/// </summary>
		[NonSerialized]
		private int m_pageIndex = 0;

		[NonSerialized]
		private bool m_initialized = false;

		[NonSerialized]
		Dictionary<xsd_ct_TableSearch[],
		  List<KeyValuePair<xsd_ct_Iteration, xsd_ct_Column>>> m_combinedCalculationColumns =
		  new Dictionary<xsd_ct_TableSearch[],
			List<KeyValuePair<xsd_ct_Iteration, xsd_ct_Column>>>();

		[NonSerialized]
		private int m_rowsPerformedCombinedCalculations = 0;

		[NonSerialized]
		private List<TableRowEntry> m_deletedRows = new List<TableRowEntry>();

		[NonSerialized]
		private bool m_saving = false;

		/// <summary>
		/// List of all cells that contain unsaved changes.
		/// </summary>
		[NonSerialized]
		private List<TableCellEntry> m_unsavedChanges = new List<TableCellEntry>();

		/// <summary>
		/// Cached value of UpdateData parameter. Needed if we need to update data
		/// of this table inside this object, needed if navigation table pages up/down.
		/// </summary>
		[NonSerialized]
		private Dictionary<int, string> m_cachedSearch = null;

		/// <summary>
		/// Cached value of UpdateData parameter. Needed if we need to update data
		/// of this table inside this object, needed if navigation table pages up/down.
		/// </summary>
		[NonSerialized]
		private String m_cachedKeyToMakeLetterIndex = null;

		/// <summary>
		/// Cached value of UpdateData parameter. Needed if we need to update data
		/// of this table inside this object, needed if navigation table pages up/down.
		/// </summary>
		[NonSerialized]
		private String m_cachedStartingStringFilter = null;

		/// <summary>
		/// Cached value of UpdateData parameter. 
		/// </summary>
		[NonSerialized]
		private Boolean m_cachedPartialHitEnabled = false;

		/// <summary>
		/// Cached value of UpdateData parameter. 
		/// </summary>
		[NonSerialized]
		private xsd_ct_Column m_cachedColumn = null;

		[NonSerialized]
		private TableRowEntry.RowChangedEventHandler m_rowChangedHandler = null;

		[NonSerialized]
		private int m_rowsPostUpdated = 0;

		[NonSerialized]
		volatile private bool m_dirty = true;

		[NonSerialized]
		private volatile bool m_markedForUpdate = false;

		[NonSerialized]
		private volatile bool m_updateStarted = false;

		[NonSerialized]
		private int m_rowsUpdated = 0;

		/// <summary>
		/// Used to contain run-time generated columns for table that are used to update entry header dynamic data
		/// with the same system that table entry editing (currently the run-time parameters, such as run path, for scripts).
		/// </summary>
		[NonSerialized]
		private Dictionary<string, xsd_ct_Column> m_fakeColumns = new Dictionary<string, xsd_ct_Column>();

		#endregion

		#region Properties

		/// <summary>
		/// All the property data for the table, automatically handled by serialization/schema.
		/// </summary>
		public xsd_ct_Table Properties
		{
			get { return m_properties; }
		}

		/// <summary>
		/// The column that is used to sort this row.
		/// </summary>
		public xsd_ct_Column SortByColumn
		{
			get { return m_sortByColumn; }
			set { m_sortByColumn = value; }
		}


		/// <summary>
		/// Tells if we sort entries on increasing or decreasing order.
		/// </summary>
		public bool SortOrderIncreasing
		{
			get { return m_sortOrderIncreasing; }
			set { m_sortOrderIncreasing = value; }
		}

		/// <summary>
		/// Our private elementary data, in "raw" format.
		/// </summary>
		public DataTable RawData
		{
			get { return m_rawData; }
		}

		public List<DataRow> RestrictionFilteredRows
		{
			get { return m_restrictionFilteredRows; }
		}


		public List<TableRowEntry> AllRows
		{
			get { return m_allRows; }
		}


		public List<TableRowEntry> VisibleRows
		{
			get { return m_visibleRows; }
		}


		public List<TableRowEntry> FilteredRows
		{
			get { return m_searchFilteredRows; }
		}

		public IList<xsd_ct_Column> Columns
		{
			get
			{
				if (m_cachedKeyToMakeLetterIndex != null
				 && m_cachedKeyToMakeLetterIndex.Equals(UNIQUE_NAME_ID))
				{
					return m_categoryColumnsList;
				}

				return m_properties.Column;
			}
		}

		public SortedList<string, string> EntryStartingLetters
		{
			get { return m_entryStartingLetters; }
		}

		/// <summary>
		/// Returns the count for tables rows.
		/// </summary>
		[XmlIgnore]
		public int ItemCount
		{
			get { return m_rawData.Rows.Count; }
		}

		/// <summary>
		/// How many pages there are in the table, given the count for all rows and 
		/// the count how many entries we want to show in one page.
		/// </summary>
		public int PageCount
		{
			get
			{
				return (int)(Math.Ceiling(((float)m_searchFilteredRows.Count) /
				  m_properties.RowsInOnePage));
			}
		}

		public int PageIndex
		{
			get { return m_pageIndex; }
			set
			{
				m_pageIndex = Math.Min(value, PageCount - 1);
				m_pageIndex = Math.Max(m_pageIndex, 0);
			}
		}

		public bool Initialized
		{
			get { return m_initialized; }
		}

		public int RowsPerformedCombinedCalculations
		{
			get { return m_rowsPerformedCombinedCalculations; }
		}

		/// <summary>
		/// How many changes that are not saved are there in this table.
		/// </summary>
		public int UnsavedChangesCount
		{
			get { return m_unsavedChanges.Count + m_deletedRows.Count; }
		}

		public bool Saving
		{
			get { return m_saving; }
		}

		public Dictionary<int, string> CachedSearch
		{
			get { return m_cachedSearch; }
		}

		public String CachedKeyToMakeLetterIndex
		{
			get { return m_cachedKeyToMakeLetterIndex; }
		}

		public String CachedStartingStringFilter
		{
			get { return m_cachedStartingStringFilter; }
		}

		public Boolean CachedPartialHitEnabled
		{
			get { return m_cachedPartialHitEnabled; }
		}

		public xsd_ct_Column CachedColumn
		{
			get { return m_cachedColumn; }
		}
		public int RowsUpdated
		{
			get { return m_rowsUpdated; }
		}
		public int RowsPostUpdated
		{
			get { return m_rowsPostUpdated; }
		}

		public IList<Table> UpdatedTables
		{
			get { return m_updatedTables; }
		}

		public IList<KeyValuePair<Table, xsd_ct_Column>> DependenciesOneLevel
		{
			get { return m_oneLevelDependencyTables; }
		}
		public IList<KeyValuePair<Table, xsd_ct_Column>> DependenciesRecursive
		{
			get { return m_recursiveDependencyTables; }
		}
		public bool Dirty
		{
			get { return m_dirty; }
			set
			{
				m_dirty = value;
				if (Dirty)
				{
					m_updateStarted = false;
					MarkedForUpdate = false;

					m_rowsUpdated = 0;
					m_rowsPerformedCombinedCalculations = 0;
					m_rowsPostUpdated = 0;

					foreach (xsd_ct_Column col in Columns)
					{
						if (col.Dirty)
						{
							foreach (KeyValuePair<Table, xsd_ct_Column> dependentTableColumn in m_oneLevelDependentTables)
							{
								if (dependentTableColumn.Value == col)
								{
									dependentTableColumn.Key.Dirty = Dirty;
								}
							}
						}
					}
				}
			}
		}

		public bool MarkedForUpdate
		{
			get { return m_markedForUpdate; }
			set { m_markedForUpdate = value; }
		}

		public bool ReadyToBeUpdated
		{
			get
			{
				// if already ok, need not to be updated
				if (!Dirty)
				{
					return false;
				}
				// if marked for update, already scheduled also
				if (MarkedForUpdate)
				{
					return false;
				}

				// if already updating, no need to do it just yet
				if (m_updateStarted)
				{
					return false;
				}

				// if we want to wait until user clicks the table name, were not ready
				if (!Properties.DoInitialLoading)
				{
					return false;
				}

				// if source is dirty, I can't be update either
				foreach (KeyValuePair<Table, xsd_ct_Column> dependency in m_oneLevelDependencyTables)
				{
					// only do this if this is a "real" dependency (not used to just update the other, which can go cyclically)
					if (dependency.Value == null)
						continue;

					if (dependency.Key.Dirty)
					{
						return false;
					}
				}
				// ... yes!
				return true;
			}
		}

		#endregion

		#region Classes Helper
		public class TableEventArgs : EventArgs
		{
			public TableEventArgs(TableRowEntry row)
			{
				this.row = row;
			}
			[NonSerialized]
			public TableRowEntry row;
		}

		#endregion

		#region Methods

		public Table(xsd_ct_Table properties)
		{
			m_properties = properties;

			m_rowChangedHandler = new TableRowEntry.RowChangedEventHandler(RowChanged);
		}

		private void RowChanged(object sender, TableRowEntry.RowEventArgs e)
		{

			TableRowEntry row = sender as TableRowEntry;
			if (row.Deleted && !m_deletedRows.Contains(row))
			{
				m_deletedRows.Add(row);
			}

			OnChanged(new TableEventArgs(row));
		}

		// Invoke the Changed event; called whenever list changes
		protected virtual void OnChanged(TableEventArgs e)
		{
			if (Changed != null)
			{
				Changed(this, e);
			}
		}

		public void InitDefaultSorting()
		{
			// init order
			SortByColumn = GetColumn(m_properties.DefaultColumnToSort);
			SortOrderIncreasing = m_properties.DefaultColumnToSortIncreasing;
		}

		public void Initialize()
		{
			if (m_initialized)
			{
				return;
			}

			m_categoryColumns.Clear();
			m_categoryColumnsList.Clear();

			xsd_ct_Column unique = new xsd_ct_Column(UNIQUE_NAME_ID, typeof(string), true, 300);
			xsd_ct_Column count = new xsd_ct_Column(COUNT_ID, typeof(int), true, 100);
			m_categoryColumns.Add(UNIQUE_NAME_ID, unique);
			m_categoryColumns.Add(COUNT_ID, count);
			m_categoryColumnsList.Add(unique);
			m_categoryColumnsList.Add(count);

			if (m_properties.SourceId != null)
			{
				foreach (string sourceId in m_properties.SourceId)
				{
					DatabaseInterface db = MediaTasterData.Instance.GetDatabase(sourceId);
					if (db != null)
					{
						m_sourceDatabase = db;
						if (m_properties.InitializeDatabase)
						{
							//m_sourceDatabase.InitializeTable(m_properties.Source);
							m_rawData = m_sourceDatabase.GetTable(m_properties.Source);
						}
						break;
					}
				}
			}
			if (m_properties.SourceDirectory != null)
			{
				MediaTasterData.Instance.ProcessTableDirectorySources(m_properties.SourceDirectory);
			}

			InitDefaultSorting();

			// some preprocessing for columns that do not change runtime
			InitializeColumns();

			InitializeDependencies();

			InitializeHeaderParametrizations();

			m_initialized = true;
		}

		public void RefreshSource()
		{
			if (m_sourceDatabase != null)
			{
				//m_rawData = m_sourceDatabase.RefreshTable(m_properties.Source);
				//Memarc.Instance.UpdateActivityFeeds(m_sourceDatabase.GetSourceId());
			}
		}

		/// Compare property values (as strings)
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public static bool PropertiesEqual(object referenceObject, object comparisonObject)
		{
			Type sourceType = referenceObject.GetType();
			Type destinationType = comparisonObject.GetType();

			if (sourceType == destinationType)
			{
				System.Reflection.PropertyInfo[] sourceProperties = sourceType.GetProperties();
				foreach (System.Reflection.PropertyInfo pi in sourceProperties)
				{
					if ((sourceType.GetProperty(pi.Name).GetValue(referenceObject, null) == null
					  && destinationType.GetProperty(pi.Name).GetValue(comparisonObject, null) == null))
					{
						// if both are null, don't try to compare  (throws exception)
					}
					else if (!(sourceType.GetProperty(pi.Name).GetValue(referenceObject, null).ToString() ==
					  destinationType.GetProperty(pi.Name).GetValue(comparisonObject, null).ToString()))
					{
						// only need one property to be different to fail Equals.
						return false;
					}
				}
			}
			else
			{
				throw new ArgumentException("Comparison object must be of the same type.", "comparisonObject");
			}
			return true;
		}

		private xsd_ct_TableSearch[] GetCorrespondingCombinedCalculationColumns(
		  xsd_ct_TableSearch[] search)
		{
			// go through every already stored search to try to find a match
			foreach (xsd_ct_TableSearch[] storedCombinedSearch in m_combinedCalculationColumns.Keys)
			{
				// not even of the same size, early exit
				if (storedCombinedSearch.Length != search.Length)
				{
					continue;
				}
				// check each sub-element
				bool found = true;
				foreach (xsd_ct_TableSearch element in search)
				{
					found = false;
					foreach (xsd_ct_TableSearch storedElement in storedCombinedSearch)
					{
						if (PropertiesEqual(storedElement, element))
						{
							found = true;
							break;
						}
					}

					if (!found)
					{
						// did not find corresponding element, so this was not a match
						break;
					}
				}

				if (found)
				{
					// if we come here, then we found stored search that corresponds to the given search
					return storedCombinedSearch;
				}
			}
			// nothing found
			return null;
		}

		private void InitializeHeaderParametrizations()
		{
			if (Properties.Header == null)
				return;

			foreach (xsd_ct_Header header in Properties.Header)
			{
				/*
				if (header.Items != null)
				{
					foreach (xsd_HeaderElement element in header.Items)
					{
						if (element.Editable)
						{
							xsd_ct_Column fakeColumn = new xsd_ct_Column(element.Id, typeof(string), false, 0);
							fakeColumn.Editable = true;
							// TODO: should we have own type for this? This isn't actually a tableValue ... using this as it passes
							// EntryContentsVisualization.MouseOnCellValue:L792 - condition
							fakeColumn.EntryType = xsd_entryType.tableValue;
							m_fakeColumns.Add(element.Id, fakeColumn);
						}
					}
				}*/
			}
		}

		private void InitializeColumns()
		{
			// this is for if we have exactly same search, but different target column for different operator
			// -> we can combine them, iterate through the lines and just do different operations for
			// different columns -> big save in time

			m_combinedCalculationColumns.Clear();

			int index = 0;
			foreach (xsd_ct_Column column in Columns)
			{
				//column.Initialize(m_rawData.Columns.IndexOf(column.Name));
				column.Initialize(index++);

				if (column == null
				  || column.ListOnlyWhenAdding
				  || column.EntryType != xsd_entryType.calculation
							|| column.EntryType != xsd_entryType.sqlQuery)
				{
					continue;
				}

				// go through every listed iteration ...
				foreach (xsd_ct_Iteration iteration in column.Iteration)
				{
					string operationColumn = iteration.Column;

					xsd_ct_TableSearch[] correspondingSearch =
					  GetCorrespondingCombinedCalculationColumns(iteration.TableSearch);

					if (correspondingSearch == null)
					{
						List<KeyValuePair<xsd_ct_Iteration, xsd_ct_Column>> newList =
						  new List<KeyValuePair<xsd_ct_Iteration, xsd_ct_Column>>();
						m_combinedCalculationColumns.Add(iteration.TableSearch, newList);

						correspondingSearch = iteration.TableSearch;
					}

					KeyValuePair<xsd_ct_Iteration, xsd_ct_Column> pair =
						new KeyValuePair<xsd_ct_Iteration, xsd_ct_Column>(iteration, column);
					m_combinedCalculationColumns[correspondingSearch].Add(pair);
				}
			}
		}

		public xsd_ct_Column GetColumn(string name)
		{
			if (String.IsNullOrEmpty(name))
			{
				return null;
			}

			if (m_fakeColumns.ContainsKey(name))
			{
				return m_fakeColumns[name];
			}

			foreach (xsd_ct_Column col in Columns)
			{
				if (col.Name.Equals(name))
				{
					return col;
				}
			}
			return null;
		}

		/// <summary>
		/// Method to do some preprocessing to the table data, e.g. checking if we have
		/// some restrictions in column-spesifications. Placing the results in list,
		/// that is used to construct the table as a later step.
		/// Note! This should be then called before Update().
		/// </summary>
		private void ApplyRestrictions()
		{
			m_restrictionFilteredRows.Clear();

			// do we have actual real source data?
			if (m_rawData == null)
			{
				// no, but we might have some "fake" data that we want to display anyways (such as direct sql-searches)
				xsd_ct_Column fakeColumn = null;
				foreach (xsd_ct_Column column in Columns)
				{
					if (column.EntryType == xsd_entryType.sqlQuery)
					{
						fakeColumn = column;
						break;
					}
				}
				if (fakeColumn != null)
				{
					for (int i = 0; i < MediaTasterData.Instance.GetQueryCount(fakeColumn.SourceQuery); i++)
					{
						// this represents that we have a place holder, the actual Row and Cell representations will
						// eventually render the result correct (in InitializeRows will go forward as this is found)
						m_restrictionFilteredRows.Add(null);
					}
				}
				return;
			}

			// do we search columns or rows?
			for (int i = 0; i < m_rawData.Rows.Count; i++)
			{
				DataRow row = m_rawData.Rows[i];

//				if (row.RowState == DataRowState.Deleted
//				  || row.RowState == DataRowState.Detached)
//				{
//					continue;
//				}

				if (m_properties.Restriction != null)
				{
					bool skip = false;
					// check if we have restrictions for this table and apply them for the row
					foreach (xsd_ct_Restriction restriction in m_properties.Restriction)
					{
						// do we allow duplicates with this rule?
						if (!restriction.Duplicates)
						{
							skip = true;
							// check all columns
							foreach (string columnId in restriction.ColumnId)
							{
								if (!HasAlreadyRowWithValue(columnId, row))
								{
									// value differed with this column, so the row is ok
									skip = false;
									break;
								}
							}
							// this means that with this restrictions column, there was already an entry
							// where all columns matched, so we need to skip (we do not need to check
							// the remaining restrictions anymore, one hit is enough)
							if (skip)
							{
								break;
							}
						}
					}
					// move to next row if we want to skip
					if (skip)
					{
						continue;
					}
				}
				m_restrictionFilteredRows.Add(row);
			}
		}


		/// <summary>
		/// Precalculate all inter-table dependent stuff. This should be called only once
		/// after tables have been loaded, or when data has changed.
		/// </summary>
		public void PerformCombinedCalculations()
		{
			m_rowsPerformedCombinedCalculations = 0;
			foreach (TableRowEntry row in m_allRows)
			{
				// perform combined searches, if there are any
				// (true for every column that is type of "calculation")

				UpdateCombinedCalculations(row);
				m_rowsPerformedCombinedCalculations++;
			}
		}

		public int GetIndexForColumn(xsd_ct_Column column)
		{
			if (!m_indexToColumn.ContainsKey(column))
				return -1;
			return m_indexToColumn[column];
		}

		/// <summary>
		/// Method to build index from each tables column to our m_column-list.
		/// With this method we now how every m_rawData's entrys map to our representation
		/// of the columns.
		/// </summary>
		/// <returns></returns>
		void BuildIndexToColumnIndex()
		{
			if (m_rawData == null)
				return;

			m_indexToColumn.Clear();
			int index = 0;
			foreach (string col in m_rawData.Columns)
			{
				// do we have in our format a match for the database column?
				// if not, then a null will be placed in our mapping
				foreach (xsd_ct_Column c in Columns)
				{
					if (c.Name.Equals(col)
						&& (c.EntryType == xsd_entryType.tableValue
							|| c.EntryType == xsd_entryType.tableValueWithCondition
							|| c.EntryType == xsd_entryType.tableValueWithInverseCondition
							|| c.EntryType == xsd_entryType.bookmark))
					{
						// cache the datatype
						//c.Type = col.DataType;
						// add to index
						m_indexToColumn.Add(c, index);
						break;
					}
				}
				index++;
			}
		}

		public bool AddOrEditEntry(Dictionary<xsd_ct_Column, object> values, TableRowEntry oldRow)
		{
			// are we editing or creating new?
			//DataRow rowToEdit = (oldRow != null) ? oldRow.RawData : m_rawData.NewRow();
			DataRow rowToEdit = (oldRow != null) ? oldRow.RawData : new DataRow();

			bool modified = false;

			if (oldRow != null && m_properties.Restriction != null)
			{
				// this means we are editing, and that we possibly have 
				// a multiselection to edit
				Dictionary<int, string> multiSearch = new Dictionary<int, string>();
				foreach (xsd_ct_Column col in Columns)
				{
					if (col != null
					  && col.EntryType == xsd_entryType.tableValue)
					{
						multiSearch.Add(col.Index, oldRow.Get(col.Index).ToString());
					}
				}
				List<TableRowEntry> multiResult = SearchRaw(multiSearch, false, false);

				foreach (TableRowEntry row in multiResult)
				{
					EditEntry(rowToEdit, values, oldRow == null);
					Dirty = true;
					SaveRawChanges();
				}
			}
			else
			{
				modified = true;
				EditEntry(rowToEdit, values, oldRow == null);
				SaveRawChanges();
			}
			return modified;
		}


		/// <summary>
		/// Method to add new entry to table. Returns true if everything went ok.
		/// We do a validation for the data, e.g. that it's in the right format in according to the
		/// column type, and that we don't already have the entry in table.
		/// </summary>
		/// <param name="values"></param>
		/// <returns></returns>
		private void EditEntry(DataRow row, Dictionary<xsd_ct_Column, object> values, bool addingNew)
		{
			string multiValueString = null;
			string multiValueColumn = null;
			//int multiValueColumnIndex = -1;

			//object[] rowValues = row;
			// fill the row with the values
			foreach (xsd_ct_Column col in Columns)
			{
				if (col.EntryType != xsd_entryType.tableValue
				  	&& col.EntryType != xsd_entryType.tableValueWithCondition
					&& col.EntryType != xsd_entryType.tableValueWithInverseCondition
					&& col.EntryType != xsd_entryType.bookmark)
				{
					continue;
				}

				// this has to exist in data table
				Debug.Assert(m_rawData.Columns.Contains(col.Name), "column does not exist in data!");

				// init to previous value
				//int colIndex = m_indexToColumn[col];

				object value = null;
				if (addingNew)
				{
//					if (m_rawData.Columns[colIndex].AutoIncrement)
//					{
//						value = m_rawData.Columns[colIndex].AutoIncrementSeed +
//						  m_rawData.Columns[colIndex].AutoIncrementStep;
//					}
//					else if (m_rawData.Columns[colIndex].DefaultValue != null
//					  && m_rawData.Columns[colIndex].DefaultValue.ToString().Length > 0)
//					{
//						value = m_rawData.Columns[colIndex].DefaultValue;
//					}
				}
				else
				{
					value = row[col.Name];
				}

				// take the value out of provided input only if it really exists ...
				if (col != null && values.ContainsKey(col))
				{
					value = values[col];
				}
				// do we have a value in search cache for this?
				// (this is the case when 
				else if (CachedSearch != null && CachedSearch.ContainsKey(col.Index))
				{
					value = CachedSearch[col.Index];
				}

				// do we check possibility of multi-addition?
				if (value != null && col.MultiValueWhenAdding)
				{
					if (multiValueString != null)
					{
						// multivalues for multiple columns not allowed!
						Debug.Assert(false);
					}

					multiValueString = value.ToString();
					multiValueColumn = col.Name;
					//multiValueColumnIndex = colIndex;
				}

				// if we did not have "real" input, on some cases do some magic ...
				if (col != null && value == null)
				{
					if (col.Type == typeof(DateTime))
					{
						// insert current time as value
						value = DateTime.Now.ToString();
					}
					else if (col.Type == typeof(Boolean))
					{
						value = false;
					}
				}

				// check if we need to record changed time, if we need
				// NOTE! This is hard-coded so that we need to create of course the
				// needed columns in the sql table ourselves ...
				if (!addingNew
				  && col.ValueModifiedDataColumnName != null
				  && col.ValueModifiedDataColumnName.Length > 0
				  && !row[col.Name].ToString().Equals(value.ToString()))
				{
					Debug.Assert(m_rawData.Columns.Contains(col.ValueModifiedDataColumnName),
					  "no needed value-changed field in table: " + col.ValueModifiedDataColumnName);
					// value changed, so record the new time
					row[col.ValueModifiedDataColumnName] = DateTime.Now.ToString();
				}

				//row[colIndex] = (value == null) ? m_rawData.Columns[colIndex].DefaultValue : value;
				row[col.Name] = (value == null) ? col.DefaultValue : value;
			}

			// actually update/add the stuff to our data
			if (addingNew)
			{
				if (multiValueString != null)
				{
					string[] separators = { ";" };
					string[] entries = multiValueString.ToString().Split(separators, StringSplitOptions.RemoveEmptyEntries);
					foreach (string entry in entries)
					{
						bool processed = false;
						// is this just a number entry, where we want to insert a range of 
						// episodes/issues/etc., for example "1-4"?
						if (entry.Contains("-"))
						{
							int indexOfLine = entry.IndexOf("-");
							int start, stop;

							bool startOk = int.TryParse(entry.Substring(0, indexOfLine), out start);
							bool stopOk = int.TryParse(entry.Substring(indexOfLine + 1, entry.Length - indexOfLine - 1), out stop);

							if (startOk && stopOk && stop > start)
							{
								processed = true;
								for (int i = start; i <= stop; i++)
								{
									row[multiValueColumn] = i.ToString();
									//m_rawData.LoadDataRow(rowValues, false);
									m_rawData.AddRow(row);
								}
							}
						}
						if (!processed)
						{
							row[multiValueColumn] = entry;
							//m_rawData.LoadDataRow(rowValues, false);
							m_rawData.AddRow(row);
						}
					}
				}
				else
				{
					//m_rawData.LoadDataRow(rowValues, false);
					m_rawData.AddRow(row);
				}
			}
//			else
//			{
//				row.ItemArray = rowValues;
//			}

			Dirty = true;
		}


		/// <summary>
		/// Reset all changes made to this table that are not saved.
		/// </summary>
		public void ResetChanges()
		{
			foreach (TableCellEntry entry in m_unsavedChanges)
			{
				entry.Reset();
				//entry.Row.RawData.RejectChanges();
			}
			foreach (TableRowEntry row in m_deletedRows)
			{
				row.Deleted = false;
			}

			foreach (xsd_ct_Column col in Columns)
			{
				col.Dirty = false;
			}

			m_unsavedChanges.Clear();
			m_deletedRows.Clear();
		}


		/// <summary>
		/// Save all changes made to this table that are not saved.
		/// NOTE! This is very slow. Should not be used frequently.
		/// For some stupid reason every time database updating takes place,
		/// every row will be invalidated, so full-reconstruction of the table is needed
		/// (if we want to maintain the database row-representation row link).
		/// -> and this is a must, otherwise we can't save anymore the changed data after one save.
		/// -> we could work this a round by relying on some cell value (ID, that is), and always fetch it
		/// before update, so that we only fetch data in db but manipulate/keep it fully in own code ... 
		/// </summary>
		public void SaveChanges()
		{
			m_saving = true;
			Dictionary<DataRow, Dictionary<xsd_ct_Column, object>> affectedRows =
			  new Dictionary<DataRow, Dictionary<xsd_ct_Column, object>>();

			foreach (TableCellEntry entry in m_unsavedChanges)
			{
				// this has to exist in data table
				Debug.Assert(m_rawData.Columns.Contains(entry.Column.Name), "column does not exist in data!");

				DataRow data = entry.Row.RawData;

				if (!affectedRows.ContainsKey(data))
				{
					affectedRows[data] = new Dictionary<xsd_ct_Column, object>();
				}
				affectedRows[data][entry.Column] = entry.Value;
			}
			foreach (KeyValuePair<DataRow, Dictionary<xsd_ct_Column, object>> pair in affectedRows)
			{
				EditEntry(pair.Key, pair.Value, false);
			}

			foreach (TableRowEntry row in m_deletedRows)
			{
				RestrictionFilteredRows.Remove(row.RawData);
				m_allRows.Remove(row);
				m_visibleRows.Remove(row);
				m_searchFilteredRows.Remove(row);

				// mark for delete, DO NOT remove from row-table as otherwise 
				// updating DataAdapter won't notice that row has been marked for delete (as it would have been removed ...)
				m_rawData.RemoveRow(row.RawData);
			}

			// we need to save here, as not all changes get updated otherwise
			SaveRawChanges();

			m_unsavedChanges.Clear();
			m_deletedRows.Clear();

			foreach (xsd_ct_Column col in Columns)
			{
				col.Dirty = false;
			}

			// need to invalidate also marked tables
			foreach (Table dep in UpdatedTables)
			{
				// database might have changed, we need to refresh it
				dep.RefreshSource();
				dep.Dirty = true;
			}

			Dirty = true;
			m_saving = false;
		}

		/// <summary>
		/// Saves all changes in table and commits them to database/file.
		/// </summary>
		public void SaveRawChanges()
		{
			if (m_sourceDatabase != null)
			{
				// take changes
				//DataTable changes = m_rawData.GetChanges();
				// make sure right table is selected
				//m_sourceDatabase.UpdateChangedValues(m_properties.Source, changes);

				RefreshSource();
			}
			// @todo: TODO: handle file, if that is the source
		}

		/// <summary>
		/// Updates the state changed information cache for this table.
		/// </summary>
		/// <param name="wasChanged"></param>
		/// <param name="isChanged"></param>
		public void UpdateItemChangedState(TableCellEntry entry)
		{
			bool contains = m_unsavedChanges.Contains(entry);
			bool changed = entry.IsChanged();

			if (changed && !contains)
			{
				m_unsavedChanges.Add(entry);
			}
			else if (!changed && contains)
			{
				m_unsavedChanges.Remove(entry);
			}
		}

		private bool HasColumn(string key)
		{
			foreach (xsd_ct_Column col in Columns)
			{
				if (col.Name.Equals(key))
				{
					return true;
				}
			}
			return false;
		}


		/// <summary>
		/// Updates the list that contains all the starting letters
		/// for tables entries, for a column that matches the given key.
		/// </summary>
		/// <param name="key"></param>
		private void UpdateStartingLetterTable(string key)
		{
			if (!HasColumn(key))
			{
				return;
			}

			m_entryStartingLetters.Clear();
			foreach (TableRowEntry row in m_visibleRows)
			{
				/*
				if (!row.ContainsKey(key))
				{
				  continue;
				}*/

				xsd_ct_Column col = GetColumn(key);
				string value = row.Get(col.Index).ToString();

				if (value == null || value.Length == 0)
				{
					value = INDEX_EMPTY_ENTRY_LETTER_LINK;
				}
				else
				{
					value = value.Substring(0, 1).ToLower();
				}
				if (!m_entryStartingLetters.ContainsKey(value))
				{
					m_entryStartingLetters[value] = key;
				}
			}
		}

		/// <summary>
		/// Go through the whole table and collect all the values that are unique for given
		/// column name, alongside how many of those there were in the table.
		/// </summary>
		/// <param name="columnName"></param>
		/// <returns></returns>
		private Dictionary<String, int> CollectUniqueValuesWithCounts(string columnName)
		{
			Dictionary<String, int> valueCounts = new Dictionary<String, int>();

			// check if row has a new value for the wanted column
			foreach (DataRow row in m_rawData.Rows)
			{
				String cellValue = row[columnName].ToString();

				if (valueCounts.ContainsKey(cellValue))
				{
					// we already had this, increment the count
					valueCounts[cellValue] = valueCounts[cellValue] + 1;
				}
				else
				{
					// yes, add as new
					valueCounts.Add(cellValue, 1);
				}
			}
			return valueCounts;
		}

		/// <summary>
		/// Here we update the table contexts (all cell values) when a column topic has been
		/// pressed. In such case we want to take the unique values of that clicked column from
		/// each row, and print those along side with a count how much of those values there were.
		/// </summary>
		public void UpdateForColumnValues()
		{
			m_visibleRows.Clear();

			// we have a certain column in cache, that we want to take and construct
			// from all the values of that column (for example, the "genre"s) a new table with those
			// values as items
			Dictionary<String, int> valueCounts = CollectUniqueValuesWithCounts(m_cachedColumn.Name);

			// now, make a row of all values
			foreach (KeyValuePair<String, int> pair in valueCounts)
			{
				TableRowEntry entry = new TableRowEntry(null, this);
				entry.Changed += m_rowChangedHandler;

				entry.Add(pair.Key, null);
				entry.Add(pair.Value, null);

				m_visibleRows.Add(entry);
			}

			// update column value column
			m_categoryColumns[UNIQUE_NAME_ID].PrintName = m_cachedColumn.Name;
		}

		public void SortAndSkipRows(string keyToMakeLetterIndex, string startingStringFilter)
		{
			m_cachedStartingStringFilter = startingStringFilter;

			if (m_visibleRows.Count > 0)
			{
				m_visibleRows.Sort();
			}

			// 3. lastly, do some additional filtering and construct the list we are
			// actually going to show
			int valuesToSkip = m_pageIndex * m_properties.RowsInOnePage;
			m_searchFilteredRows.Clear();

			String filterLowCase = null;
			if (!String.IsNullOrEmpty(startingStringFilter))
			{
				filterLowCase = startingStringFilter.ToLower();
			}

			xsd_ct_Column col = GetColumn(keyToMakeLetterIndex);

			foreach (TableRowEntry entry in m_visibleRows)
			{
				bool notNull = keyToMakeLetterIndex != null
				  && entry.Get(col.Index) != null;

				if (filterLowCase == null
				  || filterLowCase.Equals(ALL_ENTRIES_FILTER_ID)
				  || (notNull && entry.Get(col.Index).ToString().ToLower().StartsWith(filterLowCase))
				  || (filterLowCase.Equals(INDEX_EMPTY_ENTRY_LETTER_LINK) && notNull &&
					  entry.Get(col.Index).ToString().Length == 0)
				  )
				{
					m_searchFilteredRows.Add(entry);
				}
			}

			// do we make index?
			if (keyToMakeLetterIndex != null && keyToMakeLetterIndex.Length > 0)
			{
				UpdateStartingLetterTable(keyToMakeLetterIndex);
			}
		}

		/// <summary>
		/// Method that updates calculated data, probably depending other tables values.
		/// </summary>

		public void PostUpdate()
		{
			m_rowsPostUpdated = 0;
			for (int i = 0; i < m_allRows.Count; i++)
			{
				m_allRows[i].PostUpdate();
				m_rowsPostUpdated++;
			}
		}

		/// <summary>
		/// Calls the Update method with last cached values.
		/// </summary>
		public void UpdateCached()
		{
			Update(
			  CachedSearch, CachedPartialHitEnabled,
			  CachedKeyToMakeLetterIndex, CachedStartingStringFilter,
			  CachedColumn, false);
		}

		public bool IsInSearch(Dictionary<int, string> search)
		{
			if (CachedSearch == null)
				return false;

			foreach (KeyValuePair<int, string> pair in search)
			{
				if (!CachedSearch.ContainsKey(pair.Key) || CachedSearch[pair.Key] != pair.Value)
					return false;
			}
			return true;
		}

		/// <summary>
		/// Update all the data from table. Also used to make searches,
		/// when the data is only updates so that it matches the
		/// wanted search (given in a dictionary with a column name - value pairs.
		/// </summary>
		public void Update(
			Dictionary<int, string> search,
			Boolean partialHitEnabled,
			String keyToMakeLetterIndex,
			String startingStringFilter,
			xsd_ct_Column columnClicked,
			bool cacheSearch)
		{
			// store the update parameters in case we need to refresh data here by ourselves
			if (cacheSearch)
			{
				m_cachedSearch = search;
				m_cachedKeyToMakeLetterIndex = keyToMakeLetterIndex;
				m_cachedStartingStringFilter = startingStringFilter;
				m_cachedPartialHitEnabled = partialHitEnabled;
				m_cachedColumn = columnClicked;
			}
			else
			{
				InitDefaultSorting();
			}

			if (Dirty)
			{
				FullUpdate();
			}

			// if it was column that was clicked, we fill table with column stuff
			if (columnClicked != null)
			{
				SortByColumn = m_categoryColumns[UNIQUE_NAME_ID];
				UpdateForColumnValues();
			}
			else
			{
				UpdateForRowValues(
				  search,
				  partialHitEnabled,
				  keyToMakeLetterIndex,
				  startingStringFilter,
				  columnClicked);
			}

			// then, perform sorting for the search-pruned data on wanted manner
			SortAndSkipRows(keyToMakeLetterIndex, startingStringFilter);
		}

		private List<TableRowEntry> SearchRaw(IDictionary<int, string> search, bool partialHits, bool stopAtFirstHit)
		{
			List<TableRowEntry> result = new List<TableRowEntry>();
			foreach (TableRowEntry row in m_allRows)
			{
				if (Search(search, row, partialHits))
				{
					result.Add(row);
					if (stopAtFirstHit)
					{
						break;
					}
				}
			}
			return result;
		}

		private List<TableRowEntry> SearchRestricted(IDictionary<int, string> search, bool partialHits, bool stopAtFirstHit)
		{
			List<TableRowEntry> result = new List<TableRowEntry>();
			foreach (TableRowEntry row in m_allRows)
			{
				if (Search(search, row, partialHits))
				{
					result.Add(row);
					if (stopAtFirstHit)
					{
						break;
					}
				}
			}
			return result;
		}

		public List<TableRowEntry> SearchVisible(IDictionary<int, string> search, bool partialHits, bool stopAtFirstHit)
		{
			List<TableRowEntry> result = new List<TableRowEntry>();
			foreach (TableRowEntry entry in m_visibleRows)
			{
				if (Search(search, entry, partialHits))
				{
					result.Add(entry);
					if (stopAtFirstHit)
					{
						break;
					}
				}
			}
			return result;
		}

		private bool Search(IDictionary<int, string> search, TableRowEntry entry, bool partialHits)
		{
			// enabled partial hits?
			if (partialHits)
			{
				// go through all the selected column categories, included therefore in search
				foreach (int key in search.Keys)
				{
					String cellValue = entry.Get(key).ToString();

					// partial match for the search?
					if (cellValue == null || cellValue.ToLower().IndexOf(search[key].ToLower()) < 0)
					{
						// no, skip
						return false;
					}
				}
			}
			else
			{
				// here we don't allow partial hits, must be exact
				foreach (int key in search.Keys)
				{
					/*
					String cellValue = entry.Get(key).ToString();
					// exact match?
					if (!search[key].Equals(cellValue))
					{
					  // no, skip
					  return false;
					}*/
					// exact match?
					if (entry.Get(key).CompareTo(search[key]) != 0)
					{
						// no, skip
						return false;
					}
				}
			}
			// match!
			return true;
		}


		/// <summary>
		/// Checks through the already formed table entries and checks if we
		/// have such row there that matches to the one given as parameter (data row and column).
		/// Used to restrict if we can occupy the table with duplicate values.
		/// </summary>
		/// <param name="columnId"></param>
		/// <param name="row"></param>
		/// <returns></returns>
		private bool HasAlreadyRowWithValue(string columnId, DataRow row)
		{
			//int index = m_rawData.Columns.IndexOf(columnId);
			//string rowValue = row.ItemArray[index].ToString();
			string rowValue = row[columnId].ToString();

			foreach (DataRow entry in m_restrictionFilteredRows)
			{
				//if (entry.ItemArray[index].ToString().Equals(rowValue))
				if (entry[columnId].ToString().Equals(rowValue))
				{
					return true;
				}
			}
			return false;
		}


		public void AddDependency(Table sourceTable, xsd_ct_Column sourceColumn, xsd_ct_Column targetColumn)
		{
			m_oneLevelDependencyTables.Add(new KeyValuePair<Table, xsd_ct_Column>(sourceTable, sourceColumn));
			sourceTable.m_oneLevelDependentTables.Add(new KeyValuePair<Table, xsd_ct_Column>(this, targetColumn));
		}

		private void UpdateDependenciesRecursive(ref List<KeyValuePair<Table, xsd_ct_Column>> dependencies)
		{
			foreach (KeyValuePair<Table, xsd_ct_Column> oneLevelTableColumn in m_oneLevelDependencyTables)
			{
				Table table = oneLevelTableColumn.Key;

				bool add = true;
				foreach (KeyValuePair<Table, xsd_ct_Column> entry in dependencies)
				{
					if (entry.Key == table)
					{
						add = false;
						break;
					}
				}
				if (add)
				{
					dependencies.Add(oneLevelTableColumn);
					if (table.DependenciesOneLevel.Count > 0)
					{
						table.UpdateDependenciesRecursive(ref dependencies);
					}
				}
			}
		}

		public void InitThreadPoolCallback(Object threadContext)
		{
			int threadIndex = (int)threadContext;

			Console.WriteLine(String.Format("[{0}] Started calling InitThreadPoolCallback for (thread {1})", m_properties.Name, threadIndex));

			if (!m_updateStarted)
			{
				m_updateStarted = true;
				FullUpdate();
				m_updateStarted = false;
				m_markedForUpdate = false;
			}

			Console.WriteLine(String.Format("[{0}] Done calling InitThreadPoolCallback for (thread {1})", m_properties.Name, threadIndex));

			// notify, and possibly reschedule
			MediaTasterData.Instance.AddChangedTable(m_properties.Name);
		}

		public float GetInitDoneFraction()
		{
			// if i'm not dirty, i'm ready
			if (!Dirty)
			{
				return 1.0f;
			}

			float result = 0.0f;
			float multiplier = 1.0f / (m_oneLevelDependencyTables.Count + 1);

			foreach (KeyValuePair<Table, xsd_ct_Column> dep in m_oneLevelDependencyTables)
			{
				Table table = dep.Key;
				// add childs done-ratio divided by all children, note that recursion deals here the case where we have grand-children
				result += table.GetInitDoneFraction() * multiplier;
			}

			if (m_allRows.Count == 0)
			{
				return result;
			}

			result += multiplier * (RowsUpdated + RowsUpdated + RowsPerformedCombinedCalculations) / (3 * ItemCount);

			return result;
		}

		/// <summary>
		/// </summary>
		private void InitializeDependencies()
		{
			m_oneLevelDependencyTables.Clear();

			foreach (KeyValuePair<xsd_ct_TableSearch[],
			  List<KeyValuePair<xsd_ct_Iteration, xsd_ct_Column>>> pair in m_combinedCalculationColumns)
			{
				string targetTableName = null;
				string targetColumnName = null;
				string sourceColumnName = null;
				foreach (xsd_ct_TableSearch tableSearch in pair.Key)
				{
					targetTableName = tableSearch.Target.Table;
					targetColumnName = tableSearch.Target.Column;
					sourceColumnName = tableSearch.Source.Column;
				}
				if (targetTableName != null && targetColumnName != null && sourceColumnName != null)
				{
					Table targetTable = MediaTasterData.Instance.GetTable(targetTableName);
					AddDependency(targetTable
					  , targetTable.GetColumn(targetColumnName)
					  , GetColumn(sourceColumnName));
				}
			}
			// also, need to go through direct config marked dependencies
			if (Properties.UpdatedTable != null)
			{
				foreach (string tableName in Properties.UpdatedTable)
				{
					Table targetTable = MediaTasterData.Instance.GetTable(tableName);
					if (!m_updatedTables.Contains(targetTable))
					{
						m_updatedTables.Add(targetTable);
					}
				}
			}

			UpdateDependenciesRecursive(ref m_recursiveDependencyTables);
		}

		public void FullUpdate()
		{
			BuildIndexToColumnIndex();
			ApplyRestrictions();
			InitializeRows();

			UpdateRows();

			// TODO: tee t�st� kakku-versio, koska turhaan t�t� aina ajaa ... arvot kuitenkin harvoin muuttuu ja vie vitusti
			//PerformCombinedCalculations();

			PostUpdate();

			Dirty = false;

			UpdateCached();
		}

		/// <summary>
		/// This method goes through every calculation-column for row, and
		/// checks if the searches/calculations can be combined, so that we can
		/// perform multiple calculations with one search. This happens if we have
		/// same search criterias, but target to calculations for different columns.
		/// </summary>
		/// <param name="row"></param>
		private void UpdateCombinedCalculations(TableRowEntry row)
		{
			foreach (
			  KeyValuePair<xsd_ct_TableSearch[],
			  List<KeyValuePair<xsd_ct_Iteration, xsd_ct_Column>>> pair in m_combinedCalculationColumns)
			{
				xsd_ct_Column countColumn = null;
				xsd_ct_Column aveColumn = null;
				xsd_ct_Column minColumn = null;
				xsd_ct_Column maxColumn = null;

				// @todo: TODO: this propably could be moved to preprocess ... (without handling of data, that is)
				foreach (KeyValuePair<xsd_ct_Iteration, xsd_ct_Column> iteColPair in pair.Value)
				{
					xsd_ct_Iteration iteration = iteColPair.Key;
					xsd_ct_Column column = iteColPair.Value;

					if (iteration.Type == xsd_iterationType.Count)
					{
						countColumn = column;
					}
					else if (iteration.Type == xsd_iterationType.Average)
					{
						aveColumn = column;
					}
					else if (iteration.Type == xsd_iterationType.Min)
					{
						minColumn = column;
					}
					else if (iteration.Type == xsd_iterationType.Max)
					{
						maxColumn = column;
					}
				}

				if (countColumn == null && aveColumn == null && minColumn == null && maxColumn == null)
				{
					continue;
				}

				Table targetTable = null;

				IDictionary<int, string> calcSearch = new Dictionary<int, string>();

				foreach (xsd_ct_TableSearch tableSearch in pair.Key)
				{
					// take the searched value
					string sourceTableName = tableSearch.Source.Table;
					Table sourceTable = MediaTasterData.Instance.GetTable(sourceTableName);
					string sourceColName = tableSearch.Source.Column;
					xsd_ct_Column sourceCol = sourceTable.GetColumn(sourceColName);

					string colValue = row.Get(sourceCol.Index).ToString();
					// add conditions to construct query
					string targetTableName = tableSearch.Target.Table;
					string targetColName = tableSearch.Target.Column;
					targetTable = MediaTasterData.Instance.GetTable(targetTableName);
					xsd_ct_Column targetCol = targetTable.GetColumn(targetColName);

					calcSearch.Add(targetCol.Index, colValue);
				}

				if (targetTable != null)
				{
					ArrayList values = new ArrayList();
					values.Add(0); // initial count for results          
					values.Add(0); // initial count for average values
					values.Add(0.0f); // initial average
					values.Add(int.MaxValue); // initial min-value
					values.Add(int.MinValue); // initial max-value

					targetTable.PerformOperations(
					  calcSearch,
					  aveColumn, minColumn, maxColumn,
					  ref values);

					if (!m_combinedCalculationResult.ContainsKey(row))
					{
						m_combinedCalculationResult.Add(row, new Dictionary<xsd_ct_Column, object>());
					}
					Dictionary<xsd_ct_Column, object> dict = m_combinedCalculationResult[row];

					if (countColumn != null)
					{
						dict[countColumn] = values[0];
					}
					if (aveColumn != null)
					{
						dict[aveColumn] = values[2];
					}
					if (minColumn != null)
					{
						dict[minColumn] = values[3];
					}
					if (maxColumn != null)
					{
						dict[maxColumn] = values[4];
					}
				}
			}
		}

		public object GetCombinedCalculation(TableRowEntry row, xsd_ct_Column column)
		{
			if (m_combinedCalculationResult.ContainsKey(row)
			  && m_combinedCalculationResult[row].ContainsKey(column))
			{
				return m_combinedCalculationResult[row][column];
			}
			return null;
		}

		private bool MakeGroups(string entry, ref List<string> groups)
		{
			string[] elements = entry.Split(new string[] { " | " }, StringSplitOptions.RemoveEmptyEntries);
			string categorySoFar = "";
			bool leafVisible = true;

			for (uint i = 0; i < elements.Length; i++)
			{
				categorySoFar += (i > 0) ? " | " : "";
				categorySoFar += elements[i];

				if (!m_groupVisibilityMemory.ContainsKey(categorySoFar))
				{
					// the base, e.g. lowest, level is always visible, otherwise would be hard to show anything
					m_groupVisibilityMemory[categorySoFar] = (i == 0);
				}
				if (!m_groupVisibilityMemory[categorySoFar])
				{
					leafVisible = false;
				}
				if (i < elements.Length - 1 && m_groupVisibilityMemory[categorySoFar])
				{
					groups.Add(categorySoFar);
				}
			}
			return leafVisible
			  && m_groupVisibilityMemory.ContainsKey(entry)
			  && m_groupVisibilityMemory[entry];
		}

		public void InitializeRows()
		{
			m_allRows.Clear();

			// go through all actual database rows for the search
			foreach (DataRow row in m_restrictionFilteredRows)
			{
				TableRowEntry entry = new TableRowEntry(row, this);
				entry.Changed += m_rowChangedHandler;

				foreach (xsd_ct_Column column in Columns)
				{
					if (column == null)
					{
						continue;
					}
					entry.Add(null, column);
				}
				m_allRows.Add(entry);
			}
		}

		/// <summary>
		/// Update all the data from table.
		/// </summary>
		public void UpdateRows()
		{
			m_rowsUpdated = 0;
			for (int i = 0; i < m_allRows.Count; i++)
			{
				TableRowEntry row = m_allRows[i];
				row.UpdateCells(m_indexToColumn);
				m_rowsUpdated++;
			}
		}

		/// <summary>
		/// Update all the data from table. Also used to make searches,
		/// when the data is only updates so that it matches the
		/// wanted search (given in a dictionary with a column name - value pairs.
		/// </summary>
		public void UpdateForRowValues(Dictionary<int, string> search,
			Boolean partialHitEnabled,
			String keyToMakeLetterIndex,
			String startingStringFilter,
			xsd_ct_Column columnClicked)
		{
			m_visibleRows.Clear();

			for (int i = 0; i < m_allRows.Count; i++)
			{
				TableRowEntry entry = m_allRows[i];
				// check if we have searched for certain values
				if (search != null && search.Count > 0)
				{
					if (!Search(search, entry, partialHitEnabled))
					{
						// search conditions did not meet
						continue;
					}
				}

				// add if search match or no search at all
				m_visibleRows.Add(entry);
			}
		}

		static bool UnorderedEqual<T>(ICollection<T> a, ICollection<T> b)
		{
			// 1
			// Require that the counts are equal
			if (a.Count != b.Count)
			{
				return false;
			}
			// 2
			// Initialize new Dictionary of the type
			Dictionary<T, int> d = new Dictionary<T, int>();
			// 3
			// Add each key's frequency from collection A to the Dictionary
			foreach (T item in a)
			{
				int c;
				if (d.TryGetValue(item, out c))
				{
					d[item] = c + 1;
				}
				else
				{
					d.Add(item, 1);
				}
			}
			// 4
			// Add each key's frequency from collection B to the Dictionary
			// Return early if we detect a mismatch
			foreach (T item in b)
			{
				int c;
				if (d.TryGetValue(item, out c))
				{
					if (c == 0)
					{
						return false;
					}
					else
					{
						d[item] = c - 1;
					}
				}
				else
				{
					// Not in dictionary
					return false;
				}
			}
			// 5
			// Verify that all frequencies are zero
			foreach (int v in d.Values)
			{
				if (v != 0)
				{
					return false;
				}
			}
			// 6
			// We know the collections are equal
			return true;
		}

		private void PerformOperations(
		  IDictionary<int, string> search,
		  xsd_ct_Column avColumn,
		  xsd_ct_Column minColumn,
		  xsd_ct_Column maxColumn,
		  ref ArrayList previous)
		{
			// previous:
			// 0 = count
			// 1 = average item count
			// 2 = current average
			// 3 = min
			// 4 = max

			// get entries
			List<TableRowEntry> resultRows = SearchRestricted(search, false, false);

			previous[0] = resultRows.Count + (int)previous[0];
			int aveCount = 0;
			float cumulatedValue = 0.0f;

			// do operations for results
			foreach (TableRowEntry searchRow in resultRows)
			{
				object avEntry = null;
				object minEntry = null;
				object maxEntry = null;

				if (avColumn != null) avEntry = searchRow.Get(avColumn.Index).Value;
				if (minColumn != null) minEntry = searchRow.Get(minColumn.Index).Value;
				if (maxColumn != null) maxEntry = searchRow.Get(maxColumn.Index).Value;

				if (avEntry != null)
				{
					cumulatedValue += (float)avEntry;
					aveCount++;
				}
				if (minEntry != null)
				{
					previous[3] = Math.Min((int)previous[3], (int)minEntry);
				}
				if (maxEntry != null)
				{
					previous[4] = Math.Max((int)previous[4], (int)minEntry);
				}
			}

			if (aveCount > 0)
			{
				int prevAveCount = (int)previous[1];
				float prevAverage = (float)previous[2];

				previous[2] = (prevAverage * prevAveCount + cumulatedValue) / (prevAveCount + aveCount);
				previous[1] = prevAveCount + aveCount;
			}
		}

		public void SetInitialLoad()
		{
			// as a first thing, set myself dirty so that updating happens in right order
			Dirty = true;

			// recursively do this also for all dependencies
			foreach (KeyValuePair<Table, xsd_ct_Column> dependency in m_oneLevelDependencyTables)
			{
				// only do this if this is a "real" dependency (not used to just update the other, which can go cyclically)
				if (dependency.Value == null)
					continue;

				if (!dependency.Key.Properties.DoInitialLoading)
				{
					dependency.Key.SetInitialLoad();
				}
			}
			Properties.DoInitialLoading = true;

			// Forces the update, NOTE that this is just here because in xml-config it might be that we have set we do not 
			// want the initial loading to happen ... (to speed up start up)
			MediaTasterData.Instance.AddChangedTable(Properties.Name);
		}

		/*
		public String Serialize()
		{
		  return XmlUtils.SerializeObject(this, typeof(Table));
		}

		public static Table DeSerialize(XmlNode node)
		{
		  return XmlUtils.DeserializeObject(node.ToString(), typeof(Table)) as Table;
		}
		 */
	}

		#endregion
}
