
namespace MediaTaster.Model
{
	using System;
	using System.Collections.Generic;
	using System.Text;
	//using System.Drawing;

	using MediaTaster.Model;
	//using Maebius.Utility;

	using System.Runtime.Serialization.Formatters.Binary;
	using System.Runtime.Serialization;
	using System.Xml.Serialization;

	public partial class xsd_ct_Column
	{

		#region Members Not to be serializabled.
		/*
		[NonSerialized]
		private Dictionary<string, System.Drawing.Icon> m_icons
	  = new Dictionary<string, System.Drawing.Icon>();
		[NonSerialized]
		private System.Drawing.Icon m_defaultIcon = null;*/
		[NonSerialized]
		private int m_index = -1;
		[NonSerialized]
		private List<TableCellEntry> m_dirtyList = new List<TableCellEntry>();
		[NonSerialized]
		private Type m_type;
		[NonSerialized]
		private List<string> m_enumValues = new List<string>();
		[NonSerialized]
		private float m_x;
		[NonSerialized]
		private bool m_includeInSearch = false;

		#endregion

		#region Properties.
		/*
	[XmlIgnore]
	public System.Drawing.Icon DefaultIcon
	{
	get { return m_defaultIcon; }
	}*/

		[XmlIgnore]
		public int Index
		{
			get { return m_index; }
		}

		/// <summary>
		/// The data type for the column entry.
		/// </summary>
		[XmlIgnore]
		public Type Type
		{
			get { return m_type; }
			set { m_type = value; }
		}

		/// <summary>
		/// All the unique values that are used as this columns values (in the table), if this
		/// column has it's m_isEnum set to true. In that case these values are used as alternatives
		/// for adding new items to table.
		/// </summary>
		[XmlIgnore]
		public List<string> EnumValues
		{
			get { return m_enumValues; }
		}

		/// <summary>
		/// The starting x coordinate of this column. All the values of this column start from this x 
		/// as well.
		/// </summary>
		[XmlIgnore]
		public float X
		{
			get { return m_x; }
			set { m_x = value; }
		}

		/// <summary>
		/// Flag to used in runtime, if we include this column to search that we make. E.g. used can through UI
		/// toggle this on/off.
		/// </summary>
		[XmlIgnore]
		public bool IncludeInSearch
		{
			get { return m_includeInSearch; }
			set { m_includeInSearch = value; }
		}

		[XmlIgnore]
		public bool Dirty
		{
			get { return m_dirtyList.Count > 0; }
			set
			{
				if (!value)
				{
					m_dirtyList.Clear();
				}
			}
		}

		#endregion

		public xsd_ct_Column(string name, Type type, bool isVisible, int width)
		{
			Name = name;
			PrintName = name;
			Type = type;
			IsVisible = isVisible;
			Width = width;

		}

		/*
		public System.Drawing.Icon GetIcon(string value)
		{
		  if (m_icons.ContainsKey(value))
			return m_icons[value];
		  return null;
		}*/

		public void UpdateDirty(TableCellEntry cell)
		{
			if (cell.IsChanged())
			{
				if (!m_dirtyList.Contains(cell))
				{
					m_dirtyList.Add(cell);
				}
			}
			else
			{
				if (m_dirtyList.Contains(cell))
				{
					m_dirtyList.Remove(cell);
				}
			}
		}

		public void Initialize(int index)
		{
			m_index = index;

			m_enumValues.Clear();

			if (defaultValueField != null)
			{
				// Fill enum-values from config
				foreach (string val in defaultValueField)
				{
					AddEnumValue(val);
				}
			}
			/*
		  if (Icon != null)
		  {
			foreach (xsd_ct_Icon i in Icon)
			{
			  System.Drawing.Icon icon = null;
			  if (!String.IsNullOrEmpty(i.FromRegistry))
			  {
				string appExe = Browser.GetAppExeFromRegistry(i.FromRegistry);
				if (!string.IsNullOrEmpty(appExe))
					icon = System.Drawing.Icon.ExtractAssociatedIcon(appExe);
			  }
			  else if (!String.IsNullOrEmpty(i.FromFile))
			  {
				icon = Browser.MakeIcon(Image.FromFile(i.FromFile), 32, false);
			  }
			  if (icon != null)
			  {
				if (i.Default)
				{
				  m_defaultIcon = icon;
				}
				// check if we have restrictions ...
				if (i.TargetPruning != null)
				{
				  foreach (xsd_ct_TargetPruning r in i.TargetPruning)
				  {
					foreach (string s in r.RequiredValue)
					{
					  m_icons[s] = icon;
					}
				  }
				}
			  }
			}
		  }*/
		}
		/*
		public Brush DecideColors(xsd_ct_Color[] colors, TableRowEntry row)
		{
		  string selectedName = null;
		  // go through all defined conditions ...
		  if (colors != null)
		  {
			foreach (xsd_ct_Color color in colors)
			{
			  bool useThis = true;
			  string c = color.Value;

			  if (color.Condition == null && color.ConditionInverse == null)
			  {
				selectedName = c;
			  }
			  else
			  {
							if (color.Condition != null)
							{
								foreach (xsd_ct_Mapping mapping in color.Condition)
								{
									object compareItem = row.Get((row.Table.GetColumn(mapping.Source).Index));

									// ... take the target value
									if (compareItem == null
										|| (compareItem != null && !mapping.Target.Equals(compareItem.ToString())))
									{
										useThis = false;
										break;
									}
								}
							}
							else
							{
								foreach (xsd_ct_Mapping mapping in color.ConditionInverse)
								{
									object compareItem = row.Get((row.Table.GetColumn(mapping.Source).Index));

									string compareAsString = compareItem.ToString();
									if (compareAsString == null)
									{
										compareAsString = String.Empty;
									}
									if (mapping.Target.Equals(compareAsString))
									{
										useThis = false;
										break;
									}
								}
							}
			
				if (useThis)
				{
				  selectedName = c;
				  break;
				}
			  }
			}
		  }
		  if (!String.IsNullOrEmpty(selectedName))
		  {
			return new SolidBrush(Color.FromName(selectedName));
		  }

		  return null;
		}
		*/

		/// <summary>
		/// Add a new value to the possible values for this column. If the given value already exists,
		/// nothing is done.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public bool AddEnumValue(string value)
		{
			if (m_enumValues.Contains(value))
			{
				return false;
			}
			m_enumValues.Add(value);
			// keep'em sorted
			m_enumValues.Sort();
			return true;
		}
	}
}

