//#define DEBUG_TEXTS

namespace MediaTaster.Model
{
	using System.Collections;
	using System.Collections.Generic;

	using System.Xml;
	using System.Xml.Schema;
	using System.Xml.Serialization;
	using System.IO;

	public static class XmlTools
	{
		[Obfuscar.Obfuscate]
		public static XmlReader CreateReader(byte[] xmlTextAsset, byte[] schemaTextAsset, System.Type type)
		{
#if DEBUG_TEXTS
		Debug.Log (string.Format ("XmlTools.CreateReader() xmlTextAsset: \"{0}\", schemaTextAsset: {1}"
			, xmlTextAsset ? xmlTextAsset.name : "null"
			, schemaTextAsset ? schemaTextAsset.name : "null"));
#endif

			CleanUpSerializer();

			if (xmlTextAsset == null)
				return null;

			MemoryStream assetStream = new MemoryStream(xmlTextAsset);
			if (assetStream == null)
			{
#if DEBUG_TEXTS
			Debug.Log (string.Format ("XmlTools.CreateReader() MemoryStream was null! bailing out ..."));
#endif

				return null;
			}
			sm_reader = XmlReader.Create(assetStream);
			sm_serializer = new XmlSerializer(type);


			if (schemaTextAsset != null)
			{
				XmlDocument doc = new XmlDocument();
				doc.Load(sm_reader);
				SchemaValidate(schemaTextAsset, doc);
			}

			return sm_reader;
		}


		[Obfuscar.Obfuscate]
		public static XmlDocument LoadXml(byte[] xmlFile)
		{
			MemoryStream assetStream = new MemoryStream(xmlFile);
			XmlReader reader = XmlReader.Create(assetStream);
			XmlDocument xmlDoc = new XmlDocument();
			try
			{
				xmlDoc.Load(reader);
			}
			catch (System.Exception ex)
			{
#if DEBUG_TEXTS
			Debug.Log("Error loading "+ xmlFile.name + ":\n" + ex);
#endif
			}
			finally
			{
#if DEBUG_TEXTS
			Debug.Log(xmlFile.name + " loaded");
#endif
			}

			return xmlDoc;
		}

		[Obfuscar.Obfuscate]
		public static void WriteXml(string filepath, XmlDocument xmlDoc)
		{
			if (File.Exists(filepath))
			{
				using (TextWriter sw = new StreamWriter(filepath, false, System.Text.Encoding.UTF8)) //Set encoding
				{
					xmlDoc.Save(sw);
				}
			}
		}

		[Obfuscar.Obfuscate]
		private static void XmlValidationHandler(System.Object sender, ValidationEventArgs e)
		{
			if (e.Severity == XmlSeverityType.Warning)
			{
#if DEBUG_TEXTS
			Debug.LogWarning (string.Format ("XML validation Warning: {0}", e.Message));
#endif
			}
			else if (e.Severity == XmlSeverityType.Error)
			{
				string message = "XML validation error: " + e.Message;
				throw new System.Exception(message);
			}
		}

		[Obfuscar.Obfuscate]
		private static void ValidationEventHandler(object sender, ValidationEventArgs e)
		{
			switch (e.Severity)
			{
				case XmlSeverityType.Error:
#if DEBUG_TEXTS
			Debug.LogError (string.Format ("Error: {0}", e.Message));
#endif
					break;
				case XmlSeverityType.Warning:
#if DEBUG_TEXTS
			Debug.LogWarning (string.Format ("Warning {0}", e.Message));
#endif
					break;
			}
		}

		public static XmlSerializer Serializer
		{
			get
			{
				return sm_serializer;
			}
		}
		private static XmlSerializer sm_serializer = null;
		private static XmlReader sm_reader = null;

		[Obfuscar.Obfuscate]
		public static bool Serialize(string filename, System.Type type, object obj)
		{
			CleanUpSerializer();

			sm_serializer = new XmlSerializer(type);
			XmlWriterSettings settings = new XmlWriterSettings();
			settings.Indent = true;
			settings.NewLineOnAttributes = true;
			bool ok = false;
			using (XmlWriter writer = XmlWriter.Create(filename, settings))
			{
				sm_serializer.Serialize(writer, obj);
				ok = true;
			}
			return ok;
		}

		[Obfuscar.Obfuscate]
		private static bool SchemaValidate(byte[] schemaTextAsset, XmlDocument doc)
		{
#if DEBUG_TEXTS
		Debug.Log (string.Format ("XmlTools.SchemaValidate() doc: \"{0}\", schemaTextAsset: {1}"
			, doc.Name
			, schemaTextAsset.name));
#endif
			try
			{
				MemoryStream schemaStream = new MemoryStream(schemaTextAsset);
				XmlReader schemaReader = XmlReader.Create(schemaStream);
				doc.Schemas.Add(null, schemaReader);

				ValidationEventHandler eventHandler = new ValidationEventHandler(XmlTools.ValidationEventHandler);
				doc.Validate(eventHandler);
			}
			catch (System.Exception e)
			{
				string message =
					"With xml:\t\"" + doc.Name
					+ "\"\nerror:\t\"" + e.Message;

#if DEBUG_TEXTS
			Debug.LogError (string.Format ("XmlTools.InitializeSerializerForLoading - Exception: {0}", message));
#endif
				return false;
			}
			return true;
		}

		[Obfuscar.Obfuscate]
		public static XmlReader InitializeSerializerForLoading(string filename, bool validate, byte[] schemaTextAsset, System.Type type)
		{
#if DEBUG_TEXTS
		Debug.Log (string.Format ("InitializeSerializerForLoading filename: \"{0}\" schemaResource: \"{1}\""
			, filename
			, schemaTextAsset.name));
#endif

			CleanUpSerializer();

			try
			{
				// Create an instance of the XmlSerializer specifying type and namespace.
				sm_serializer = new XmlSerializer(type);
				sm_reader = new XmlTextReader(filename);

				if (validate)
				{
					XmlDocument doc = new XmlDocument();
					doc.Load(sm_reader);
					SchemaValidate(schemaTextAsset, doc);
				}
			}
			catch (System.Exception e)
			{
				string message =
					"With xml:\t\"" + filename
					+ "\"\nerror:\t\"" + e.Message;

#if DEBUG_TEXTS
			Debug.LogError (string.Format ("XmlTools.InitializeSerializerForLoading - Exception: {0}", message));
#endif

				return null;
			}

			return sm_reader;
		}

		[Obfuscar.Obfuscate]
		public static void CleanUpSerializer()
		{
			if (sm_reader != null)
			{
				sm_reader.Close();
				sm_reader = null;
			}
			if (sm_serializer != null)
			{
				sm_serializer = null;
			}
		}

		[Obfuscar.Obfuscate]
		public static bool Deserialize(string filename, string schema, System.Type type, ref object output)
		{
			try
			{
				XmlDocument doc = new XmlDocument();
				doc.Load(filename);
				doc.Schemas.Add(null, schema);

				ValidationEventHandler eventHandler = new ValidationEventHandler(ValidationEventHandler);

				// Create an instance of the XmlSerializer specifying type and namespace.
				XmlSerializer serializer = new XmlSerializer(type);

				XmlReader reader = new XmlTextReader(filename);

				output = serializer.Deserialize(reader);

				reader.Close();

				return true;
			}
			catch (System.Exception e)
			{
				string message =
					"With xml:\t\"" + filename
					+ "\"\nand schema:\t\"" + schema
					+ "\"\nerror:\t\"" + e.Message;

#if DEBUG_TEXTS
			Debug.LogError (string.Format ("Exception: {0}", message));
#endif
			}
			return false;
		}

		[Obfuscar.Obfuscate]
		public static void NNUKE_SaveGame(string filename, System.Type type, ref System.Object output)
		{
			XmlSerializer serializer = new XmlSerializer(type);
			XmlWriterSettings settings = new XmlWriterSettings();
			settings.Indent = true;
			settings.NewLineOnAttributes = true;
			using (XmlWriter writer = XmlWriter.Create(filename, settings))
			{
				serializer.Serialize(writer, output);
			}
		}
	}
}