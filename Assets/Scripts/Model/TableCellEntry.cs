
//using Maebius.Utility;

namespace MediaTaster.Model
{
	using System;
	using System.Collections.Generic;
	using System.Text;
	//using System.Data;
	using System.IO;
	using System.Diagnostics;

	using Database;

	[Serializable]
	public class TableCellEntry
	{
		#region Classes Helper
		public class CellEventArgs : EventArgs
		{
			/// <summary>
			///  Representing empty args, implemented for convenience so no need for constant 
			///  creation of empty args.
			/// </summary>
			public static CellEventArgs Empty = new CellEventArgs();

			public CellEventArgs()
			{
				// currently empty, implemented as a stub if we need something here ...
			}
		}
		#endregion

		#region Delegates
		// A delegate type for hooking up change notifications.
		public delegate void CellChangedEventHandler(object sender, CellEventArgs e);
		#endregion

		#region Events
		// An event that clients can use to be notified whenever entry changes.
		public event CellChangedEventHandler Changed;

		#endregion

		#region Enums

		public enum Status
		{
			UnInitialized,
			OK,
			RunTimeParametersMissing,
			FilePathNotFound
		}

		#endregion

		#region Static members


		#endregion

		#region Members Serializable

		private object m_value = null;

		#endregion

		#region Members Non serializable

		[NonSerialized]
		private bool m_considerConstant = false;
		[NonSerialized]
		private object m_originalValue = null;
		[NonSerialized]
		private DateTime m_valueChangedTime = DateTime.MinValue;
		[NonSerialized]
		private Status m_linkStatus = Status.UnInitialized;

		/// <summary>
		/// If this is set to true, then next update to value will be considered as setting of the value,
		/// e.g. it has not been changed (mainly for visual and save-purposes).
		/// </summary>
		[NonSerialized]
		private bool m_considerNextAsOriginal = false;
		[NonSerialized]
		private xsd_ct_Column m_column = null;
		[NonSerialized]
		private TableRowEntry m_row = null;
		[NonSerialized]
		private List<TableCellEntry> m_dependencies = new List<TableCellEntry>();
		[NonSerialized]
		private Dictionary<string, Bookmark> m_bookmarks = new Dictionary<string, Bookmark>();

		#endregion

		#region Properties
		public Status LinkStatus
		{
			get { return m_linkStatus; }
		}
		public object Value
		{
			get { return m_value; }
			set
			{
				if (m_column.EntryType == xsd_entryType.bookmark)
				{
					string bookmarks = BookmarksString;
					UpdateBookmarks(bookmarks);
					m_value = bookmarks;
				}
				else
				{
					m_value = value;
				}

				if (m_value != null && String.IsNullOrEmpty(m_value.ToString()))
				{
					m_value = null;
				}
				if (m_considerNextAsOriginal)
				{
					m_originalValue = m_value;
					m_considerNextAsOriginal = false;
				}
				m_valueChangedTime = IsChanged() ? DateTime.Now : DateTime.MinValue;
				Column.UpdateDirty(this);

				m_cachedStringValue = null;

				// notify listeners
				OnChanged(CellEventArgs.Empty);
			}
		}
		public bool ConsiderConstant
		{
			get { return m_considerConstant; }
			set { m_considerConstant = value; }
		}


		public TableRowEntry Row
		{
			get { return m_row; }
		}


		public xsd_ct_Column Column
		{
			get { return m_column; }
		}

		public DateTime ValueChanged
		{
			get { return m_valueChangedTime; }
		}

		public Dictionary<string, Bookmark> Bookmarks
		{
			get { return m_bookmarks; }
		}

		public string BookmarksString
		{
			get
			{
				string val = "";
				int c = 0;
				foreach (Bookmark b in m_bookmarks.Values)
				{
					c++;
					val += b.ValueString;
					if (c < m_bookmarks.Count)
						val += ";";
				}
				return val;
			}
		}

		#endregion

		#region Methods

		public TableCellEntry(TableRowEntry row, xsd_ct_Column column)
		{
			m_row = row;
			m_column = column;

			Initialize();
		}

		public void ApplyChange()
		{
			m_originalValue = m_value;
			Value = m_originalValue;
		}

		public void Reset()
		{
			Value = m_originalValue;
		}

		public TableCellEntry(object value, TableRowEntry row, xsd_ct_Column column)
		{
			m_value = value;
			m_originalValue = value;
			m_row = row;
			m_column = column;

			Initialize();
		}

		private void Initialize()
		{
			switch (m_column.EntryType)
			{
				case xsd_entryType.tableValue:
				case xsd_entryType.tableValueWithCondition:
				case xsd_entryType.tableValueWithInverseCondition:
				case xsd_entryType.bookmark:
				case xsd_entryType.mapped:
					m_considerConstant = false;
					break;
				default:
					m_considerConstant = true;
					break;
			}
			m_considerNextAsOriginal = true;
		}

		public void InitializeDependencies()
		{
			m_dependencies.Clear();
			// TODO

		}

		public void AddNewBookmark()
		{
			// NOTE! Currently the new bookmark will be added with key of an fixed string,
			// so you can have only one like that and then need to change it immediately so
			// that the key will be updated
			if (m_bookmarks.ContainsKey(Bookmark.NEW_BOOKMARK_ID))
			{
				return;
			}
			m_bookmarks[Bookmark.NEW_BOOKMARK_ID] = new Bookmark(this);
			// NOTE! Setting of value handles the case of bookmarks itself, we just initiate the process by passing empty string
			Value = "";
		}

		public void RemoveBookmarkByName(string name)
		{
			string toRemove = null;
			foreach (Bookmark b in m_bookmarks.Values)
			{
				if (b.Name.Equals(name))
				{
					toRemove = b.ValueString;
					break;
				}
			}
			if (toRemove != null)
			{
				Bookmarks.Remove(toRemove);
			}
			else
			{
				Bookmarks.Remove(name);
			}
		}

		public void UpdateBookmarks(System.Object value)
		{
			m_bookmarks.Clear();
			string asString = value.ToString();

			if (String.IsNullOrEmpty(asString))
			{
				return;
			}

			string[] bookmarkEntries = asString.Split(';');
			foreach (string b in bookmarkEntries)
			{
				m_bookmarks[b] = new Bookmark(this);
				m_bookmarks[b].Parse(b);
			}
		}

		public void UpdateDependencies(Dictionary<xsd_ct_Column, int> indexToColumn)
		{
			System.Object item = null;

			DataRow rawData = m_row.RawData;
			Table table = m_row.Table;

			// different handling depending on what column we have ...
			switch (m_column.EntryType)
			{
				case xsd_entryType.mapped:
					{
						xsd_ct_Column mappedColumn = table.GetColumn(m_column.QueryIndexColumn);
						//object key = rawData.ItemArray[indexToColumn[mappedColumn]];
						object key = rawData[mappedColumn.Name];
						item = MediaTasterData.Instance.GetMapping(key);
						break;
					}
				case xsd_entryType.tableValue:
				{
					//item = rawData.ItemArray[indexToColumn[m_column]];
					item = rawData[m_column.Name];

					if (m_column.IsEnum && item != null)
					{
						m_column.AddEnumValue(item.ToString());
					}
				
					break;
				}
			
				case xsd_entryType.constant:
				{
					item = m_column.PrintIfEmpty;
					break;
				}
				case xsd_entryType.tableValueWithCondition:
				{
					if (m_column.Condition != null)
					{
						//item = rawData.ItemArray[indexToColumn[m_column]];
						item = rawData[m_column.Name];

						// go through all defined conditions ...
						foreach (xsd_ct_Mapping mapping in m_column.Condition)
						{
							xsd_ct_Column compareColumn = table.GetColumn(mapping.Source);
							if (compareColumn != null)
							{
								// ... take the target value
								//object compareItem = rawData.ItemArray[indexToColumn[compareColumn]];
								object compareItem = rawData[compareColumn.Name];
								if (!mapping.Target.Equals(compareItem.ToString()))
								{
									// ... and if we find that something does not hold, we just print empty
									item = m_column.PrintIfEmpty;
									break;
								}
							}
						}
					}
					break;
				}
				case xsd_entryType.tableValueWithInverseCondition:
				{
					if (m_column.ConditionInverse != null)
					{
						//item = rawData.ItemArray[indexToColumn[m_column]];
						item = rawData[m_column.Name];

						// go through all defined conditions ...
						foreach (xsd_ct_Mapping mapping in m_column.ConditionInverse)
						{
							xsd_ct_Column compareColumn = table.GetColumn(mapping.Source);
							if (compareColumn != null)
							{
								// ... take the target value
								//object compareItem = rawData.ItemArray[indexToColumn[compareColumn]];
								object compareItem = rawData[compareColumn.Name];

								string compareAsString = compareItem != null
									? compareItem.ToString()
									: null;

								if (compareAsString == null)
								{
									compareAsString = String.Empty;
								}
								if (mapping.Target.Equals(compareAsString))
								{
									// ... and if we find that something does not hold, we just print empty
									item = m_column.PrintIfEmpty;
									break;
								}
							}
						}
					}
					break;
				}
				case xsd_entryType.bookmark:
				{
					//item = rawData.ItemArray[indexToColumn[m_column]];
					item = rawData[m_column.Name];

					UpdateBookmarks(item);
					break;
				}
				default:
					break;
			}
			Value = item;
		}


		// Invoke the Changed event
		protected virtual void OnChanged(CellEventArgs e)
		{
			if (Changed != null)
			{
				Changed(this, e);
			}
		}

		public void PostUpdate()
		{
			switch (m_column.EntryType)
			{
			/*
				case xsd_entryType.mapped:
					{
						Table table = m_row.Table;
						xsd_ct_Column mappedColumn = table.GetColumn(m_column.QueryIndexColumn);
						int index = table.GetIndexForColumn(mappedColumn);
						if (index > -1)
						{
							object key = m_row.RawData.ItemArray[index];
							m_value = Memarc.Instance.GetMapping(key);
						}
						break;
					}*/
				case xsd_entryType.sqlQuery:
					{
						Table table = m_row.Table;

						TableCellEntry indexCell = m_row.Get((m_row.Table.GetColumn(m_column.QueryIndexColumn).Index));
					  
						System.Object item = MediaTasterData.Instance.GetQuery(
							m_column.SourceQuery, m_column.Name, indexCell.ToString());

						if (item == null)
						{
							item = m_column.PrintIfEmpty;
						}
						m_value = item;
						break;
					}
				case xsd_entryType.calculation:
					{
						Table table = m_row.Table;
						System.Object item = table.GetCombinedCalculation(m_row, m_column);
						if (item == null)
						{
							item = m_column.PrintIfEmpty;
						}
						m_value = item;
						break;
					}
				case xsd_entryType.combination:
					{
						if (m_column.Combination != null)
						{
							foreach (xsd_ct_Combination comb in m_column.Combination)
							{
								int notFoundCount = 0;
								List<string> parts = new List<string>();
								foreach (xsd_ct_Parameter parameter in comb.Parameter)
								{
									if (parameter.Type == xsd_ParameterType.RuntimeMapped)
									{
										xsd_ct_Column targetColumn = m_row.Table.GetColumn(parameter.Replace[0].Target);
										string val = MediaTasterData.Instance.GetRunTimeParameter(
											parameter.Replace[0].Source, m_row.Get(targetColumn.Index).ToString());

										if (String.IsNullOrEmpty(val))
										{
											notFoundCount++;
										}
										else
										{
											parts.Add(val);
										}
									}
									else
									{
										string simpleVal = "";
										if (parameter.Type == xsd_ParameterType.Constant)
										{
											simpleVal = parameter.Value;
										}
										else if (parameter.Type == xsd_ParameterType.TableValue)
										{
											xsd_ct_Column targetColumn = m_row.Table.GetColumn(parameter.Value);
											simpleVal = m_row.Get(targetColumn.Index).ToString();
										}

										// do we want to do some replacing before adding to result?
										if (parameter.Replace != null && parameter.Replace.Length > 0)
										{
											foreach (xsd_ct_Mapping mapping in parameter.Replace)
											{
												simpleVal = simpleVal.Replace(mapping.Source, mapping.Target);
											}
										}
										parts.Add(simpleVal);
									}
								}
								string updatedValue = "";
								foreach (string val in parts)
								{
									updatedValue += val;
								}

								if (comb.Name == "path")
								{
									m_linkStatus = Status.OK;

									
									if (notFoundCount > 0)
									{
										// we don't have the right hard disc inserted ...
										m_linkStatus = Status.RunTimeParametersMissing;
									}
									else 
									{
										// file/directory is found!
										m_linkStatus = Status.OK;
										
										// NOTE! We do not use here:
										// if (System.IO.File.Exists(updatedValue)
										// || System.IO.Directory.Exists(updatedValue))
										// ... as this is very slow at least with Parallels / OSX ...
										// -> Status.FilePathNotFound should be marked when run-time finding out that there is no file
									}
								}

								m_value = updatedValue;
							}
						}
						break;
					}
			}
			// notify listeners
			OnChanged(CellEventArgs.Empty);
		}

		public bool IsChanged()
		{
			if (m_considerConstant)
			{
				return false;
			}
			if ((m_originalValue != m_value && (m_originalValue == null || m_value == null))
				|| (m_originalValue != null && m_value != null && !m_value.ToString().Equals(m_originalValue.ToString())))
			{
				return true;
			}
			return false;
		}

		public int CompareTo(string entry)
		{
			return ToString().CompareTo(entry);
		}

		public int CompareTo(TableCellEntry entry)
		{
			if (m_value == null)
			{
				if (entry.Value == null)
				{
					return 0;
				}
				return -1;
			}
			else if (entry.Value == null)
			{
				return 1;
			}

			float numberOne;
			float numberTwo;

			if (m_value is DateTime && entry.m_value is DateTime)
			{
				DateTime thisDate = (DateTime)m_value;
				DateTime thatDate = (DateTime)entry.m_value;

				return thisDate.CompareTo(thatDate);
			}
			else if (
					float.TryParse(ToString(), out numberOne) &&
					float.TryParse(entry.ToString(), out numberTwo))
			{
				return numberOne.CompareTo(numberTwo);
			}

			string thisString = ToString();
			string thatString = entry.ToString();

			if (thisString == null || thatString == null)
			{
				throw new ArgumentException("entry is null!");
			}

			return thisString.CompareTo(thatString);
		}

		public string ToPrettyString()
		{
			if (m_value is Boolean)
			{
				bool val = (bool)m_value;
				return (val) ? "x" : "-";
			}
			string valueAsString = ToString();

			if (String.IsNullOrEmpty(valueAsString))
			{
				valueAsString = m_column.PrintIfEmpty;
			}
			return valueAsString;
		}

		private string m_cachedStringValue = null;

		override public string ToString()
		{
			if (m_cachedStringValue != null)
			{
				return m_cachedStringValue;
			}
			if (m_value == null)
			{
				return null;
			}

			// we still want to map the value to something?
			if (m_value is DateTime)
			{
				DateTime dt = (DateTime)m_value;
				m_cachedStringValue =
						dt.Year + "-" +
						((dt.Month < 10) ? "0" : "") + dt.Month + "-" +
						((dt.Day < 10) ? "0" : "") + dt.Day;
			}
			else
			{
				if (m_column.FormatAsFileSize)
				{
					m_cachedStringValue = String.Format(new FileSizeFormatProvider(), "{0:fs}", long.Parse(m_value.ToString()));
				}
				else
				{
					m_cachedStringValue = String.Format(MediaTasterData.Instance.CultureInfo, "{0}", m_value);
				}
			}


			return m_cachedStringValue;
		}

		public void HandleRegistryProcess(xsd_ct_RegistryProcess process, List<string> bookmarkParameters)
		{
			Table table = Row.Table;

			// check if we have restrictions ...
			if (process.TargetPruning != null)
			{
				foreach (xsd_ct_TargetPruning r in process.TargetPruning)
				{
					List<string> acceptedValues = new List<string>(r.RequiredValue);
					foreach (xsd_ct_Parameter param in r.Parameter)
					{
						// needs to be "real" (meaning a table variable) value,
						// otherwise there's no point doing this ...
						if (param.Type == xsd_ParameterType.TableValue)
						{
							xsd_ct_Column col = table.GetColumn(param.Value);
							string val = Row.Get(col.Index).ToString();

							if (!acceptedValues.Contains(val))
							{
								// we do not accept this value as target, so do nothing
								return;
							}
						}
					}
				}
			}

			string paramString = "";
			if (process.Parameter != null)
			{
				foreach (xsd_ct_Parameter param in process.Parameter)
				{
					string val = "";
					if (param.Value != null && param.Value.Length > 0)
					{
						if (param.Type == xsd_ParameterType.Constant)
						{
							val = param.Value;
						}
						else if (param.Type == xsd_ParameterType.BookmarkValue && bookmarkParameters != null)
						{
							// the value of this is index to bookmark values
							int index = int.Parse(param.Value);
							Debug.Assert(index >= 0 && index < bookmarkParameters.Count);
							val = bookmarkParameters[index];
						}
						else if (param.Type == xsd_ParameterType.TableValue)
						{
							xsd_ct_Column col = table.GetColumn(param.Value);
							val = Row.Get(col.Index).ToString();
						}
						else
						{
							Debug.Assert(false, "Currently not supported!");
						}
					}

					// do we want to do some replacing before adding to result?
					if (param.Replace != null && param.Replace.Length > 0)
					{
						foreach (xsd_ct_Mapping mapping in param.Replace)
						{
							val = val.Replace(mapping.Source, mapping.Target);
						}
					}
					paramString += val;
				}
			}
			//Browser.OpenProcessToCorrespondingRegistry(process.Key, paramString);
		}

		#endregion

	}
}
