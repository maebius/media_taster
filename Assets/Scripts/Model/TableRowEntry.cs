
namespace MediaTaster.Model
{
	using System;
	using System.Collections.Generic;
	using System.Collections;
	using System.Text;

	using Database;

	[Serializable]
	public class TableRowEntry : IComparable
	{
		#region Classed Helper

		public class RowEventArgs : EventArgs
		{
			/// <summary>
			///  Representing empty args, implemented for convenience so no need for constant 
			///  creation of empty args.
			/// </summary>
			public static RowEventArgs Empty = new RowEventArgs(null);

			public RowEventArgs(TableCellEntry cell)
			{
				this.cell = cell;
			}
			public TableCellEntry cell;
		}
		#endregion

		#region Enums

		public enum Status
		{
			UnInitialized,
			OK,
			RunTimeParametersMissing,
			FilePathNotFound
		}
		#endregion

		#region Delegates

		// A delegate type for hooking up change notifications.
		public delegate void RowChangedEventHandler(object sender, RowEventArgs e);

		#endregion

		#region Events

		// An event that clients can use to be notified whenever the
		// elements of the list change.
		public event RowChangedEventHandler Changed;

		#endregion

		#region Members Serializable

		private List<TableCellEntry> m_values = new List<TableCellEntry>();

		private Status m_linkStatus = Status.UnInitialized;

		#endregion

		#region Members Non serializable

		/// <summary>
		/// The table to where this row belongs
		/// </summary>
		[NonSerialized]
		private Table m_parent = null;

		/// <summary>
		/// The raw data from DataTable that this object is based on, if any.
		/// </summary>
		[NonSerialized]
		private DataRow m_rawData = null;

		[NonSerialized]
		private bool m_deleted = false;

		[NonSerialized]
		private TableCellEntry.CellChangedEventHandler m_cellChangedHandler = null;

		#endregion

		#region Properties

		public DataRow RawData
		{
			get { return m_rawData; }
			set { m_rawData = value; }
		}

		public Table Table
		{
			get { return m_parent; }
		}

		/// <summary>
		/// All the data elements of this row.
		/// </summary>        
		public List<TableCellEntry> Values
		{
			get { return m_values; }
		}

		public bool Deleted
		{
			get { return m_deleted; }
			set
			{
				m_deleted = value;
				OnChanged(RowEventArgs.Empty);
			}
		}

		public Status LinkStatus
		{
			get { return m_linkStatus; }
		}

		#endregion

		#region Methods

		override public string ToString()
		{
			string result = "";
			if (Values.Count > 0)
			{
				for (int i = 0; i < Values.Count; i++)
				{
					result += String.Format("[{0}] {1} = \"{2}\"{3}"
					  , i
					  , Values[i].Column.Name
					  , Values[i].ToPrettyString()
					  , (i == Values.Count - 1) ? "" : Environment.NewLine);
				}
			}
			return result;
		}

		/// <summary>
		/// Is some of this rows values changed?
		/// </summary>
		/// <returns></returns>
		public bool IsChanged()
		{
			//foreach (TableCellEntry cell in Values.Values)
			foreach (TableCellEntry cell in Values)
			{
				if (cell.IsChanged())
				{
					return true;
				}
			}
			return false;
		}

		/// <summary>
		/// Default constructor. Does nothing.
		/// </summary>
		public TableRowEntry(DataRow row, Table table)
		{
			m_rawData = row;
			m_parent = table;

			m_cellChangedHandler = new TableCellEntry.CellChangedEventHandler(CellChanged);
		}

		/// <summary>
		/// Comparer method for this row. It works by comparing just
		/// the strings in the row element that is defined by the
		/// SortByColumn - key.
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public int CompareTo(object obj)
		{
			TableRowEntry temp = obj as TableRowEntry;

			if (temp == null)
			{
				throw new ArgumentException("object is not a TableRowEntry");
			}

			int result = 0;
			if (m_parent.SortByColumn != null
			  && m_parent.SortByColumn.Name != null)
			{
				result = Get(m_parent.SortByColumn.Index).CompareTo(temp.Get(m_parent.SortByColumn.Index));

				// if we want decreasing order, we invert the comparison result
				if (!m_parent.SortOrderIncreasing)
				{
					result *= -1;
				}
				return result;
			}

			return result;
		}

		/// <summary>
		/// Get the row element, associated with the given key.
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		public TableCellEntry Get(int index)
		{
			if (index >= m_values.Count)
			{
				throw new ArgumentException(string.Format ("TableRowEntry.Get() - error, trying index {0}, max:{1}", 
				                         index, m_values.Count));
			}

			return m_values[index];
		}

		/// <summary>
		/// Add an entry for this row.
		/// </summary>
		/// <param name="key"></param>
		/// <param name="value"></param>
		public void Add(object value, xsd_ct_Column column)
		{
			TableCellEntry cell = new TableCellEntry(value, this, column);
			cell.Changed += m_cellChangedHandler;
			m_values.Add(cell);
		}


		private void CellChanged(object sender, TableCellEntry.CellEventArgs e)
		{
			TableCellEntry entry = sender as TableCellEntry;

			switch (entry.LinkStatus)
			{
				case TableCellEntry.Status.FilePathNotFound:
					m_linkStatus = Status.FilePathNotFound;
					break;
				case TableCellEntry.Status.RunTimeParametersMissing:
					m_linkStatus = Status.RunTimeParametersMissing;
					break;
				case TableCellEntry.Status.OK:
					m_linkStatus = Status.OK;
					break;
			}
			m_parent.UpdateItemChangedState(entry);

			// notify listeners
			OnChanged(new RowEventArgs(entry));
		}

		public void UpdateCells(Dictionary<xsd_ct_Column, int> indexToColumn)
		{
			foreach (TableCellEntry cell in m_values)
			{
				cell.UpdateDependencies(indexToColumn);
			}
		}

		public void PostUpdate()
		{
			foreach (TableCellEntry cell in m_values)
			{
				cell.PostUpdate();
			}
		}

		// Invoke the Changed event; called whenever list changes
		protected virtual void OnChanged(RowEventArgs e)
		{
			if (Changed != null)
			{
				Changed(this, e);
			}
		}

		#endregion
	}
}
