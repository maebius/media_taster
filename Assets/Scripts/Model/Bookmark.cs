
namespace MediaTaster.Model
{
	using System;
	using System.Collections.Generic;
	using System.Text;
	using System.Data;
	using System.IO;
	using System.Diagnostics;

	//using Maebius.Utility;
	public class Bookmark
  {

    #region Members Serializable

    private string m_name;

    #endregion

    #region Members Non serializable

    TableCellEntry m_cell = null;    
    xsd_ct_BookmarkTarget m_target = null;

    private string m_valueString = "";
    private List<string> m_parameters = new List<string>();

    #endregion

    #region Static members

    public static string NEW_BOOKMARK_ID = "EDIT ME!";
    #endregion

    #region Properties

    public string Name
    {
      get { return m_name; }
    }
    public xsd_ct_Column Column
    {
      get { return m_cell.Column; }
    }
    public TableCellEntry Entry
    {
      get { return m_cell; }
    }
    public string ValueString
    {
      get { return m_valueString; }
    }

    #endregion

    #region Methods

    public Bookmark(TableCellEntry cell)
    {
      m_cell = cell;
      m_name = NEW_BOOKMARK_ID;
      m_valueString = NEW_BOOKMARK_ID;
    }

    public void Parse(string inputString)
    {
      if (inputString == NEW_BOOKMARK_ID)
      {
        // this is a special case of a new bookmark, so we exit here
        // -> the prevention of just having one new bookmark is done elsewhere,
        // in AddNewBookmark() method
        return;
      }

      string[] components = inputString.ToString().Split(',');

      if (components.Length < 3)
      {
        // Need at least 3 entries! Otherwise just exit here (the old value remains)
        return;
      }

      m_valueString = inputString;

      m_name = components[0];
      string targetName = components[1];

      // find the corresponding target
      foreach (xsd_ct_BookmarkTarget trg in m_cell.Column.BookmarkTarget)
      {
        if (trg.Name.Equals(targetName))
        {
          m_target = trg;
          break;
        }
      }

      Debug.Assert(m_target != null);

      for (uint i = 2; i < components.Length; i++)
      {
        AddParameter(components[i]);
      }
    }

    public void AddParameter(string param)
    {
      m_parameters.Add(param);
    }

    public void Process()
    {
      TableRowEntry row = m_cell.Row;
      Table table = row.Table;

      if (m_target != null)
      {
        m_cell.HandleRegistryProcess(m_target.RegistryProcess, m_parameters);
      }
    }

    #endregion

  }


};