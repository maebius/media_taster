﻿namespace MediaTaster.View
{
	using System;
	using UnityEngine;
	using System.Collections.Generic;
	using System.Collections;

	using MediaTaster.Model;

	class ColumnHeaderVisualisationNGUI : MonoBehaviour
	{
		public UILabel m_label;

		public Vector2 PrintSize
		{
			get { return m_printSize; }
		}

		private xsd_ct_Column m_column;
		private Vector2 m_printSize;

		public void Initialize(Vector3 pos, xsd_ct_Column column)
		{
			transform.localPosition = pos;

			m_column = column;
			m_label.text = column.PrintName;
			// update for next column header element
			m_printSize = m_label.font.CalculatePrintedSize(m_label.text, true, UIFont.SymbolStyle.None);
		}
	}
}

