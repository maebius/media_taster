﻿namespace MediaTaster.View
{
	using System;
	using UnityEngine;
	using System.Collections.Generic;
	using System.Collections;

	using MediaTaster.Model;

	class RowVisualisationNGUI : MonoBehaviour
	{
		
		public float Height
		{
			get { return m_height; }
		}

		private TableRowEntry m_row;

		private float m_height;
		
		/// <summary>
		/// All the visualisation objects of this rows elements.
		/// </summary>
		private Dictionary<xsd_ct_Column, CellVisualisationNGUI> m_cells =
			new Dictionary<xsd_ct_Column, CellVisualisationNGUI>();

		public void Initialize(Vector3 pos, ICollection<xsd_ct_Column> columns, CellVisualisationNGUI cellPrefab)
		{
			transform.position = pos;

			// make visualisation for rows every actual visible element
			foreach (xsd_ct_Column column in columns)
			{
				if (!column.IsVisible)
					continue;

				CellVisualisationNGUI cell = (CellVisualisationNGUI)Instantiate(cellPrefab);
				cell.Initialize(column);
				m_cells[column] = cell;
			}

			Refresh(null, 0f);
		}

		public void Refresh(TableRowEntry data, float y)
		{
			float maxHeight = 0;
			m_row = data;
			
			Vector3 op = transform.localPosition;
			transform.localPosition = new Vector3(op.x, y, op.z);
			
			foreach (KeyValuePair<xsd_ct_Column, CellVisualisationNGUI> pair in m_cells)
			{
				xsd_ct_Column column = pair.Key;
				CellVisualisationNGUI vis = pair.Value;

				if (!column.IsVisible)
					continue;

				TableCellEntry cell = null;
				if (m_row != null)
				{
					cell = m_row.Get(column.Index);
				}
			
				vis.Refresh(this, cell);
				maxHeight = Math.Max(maxHeight, vis.Height);
			}
			m_height = maxHeight;
		}
	}
}