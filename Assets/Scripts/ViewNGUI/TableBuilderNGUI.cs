﻿namespace MediaTaster.View
{
	using System;
	using UnityEngine;
	using System.Collections.Generic;
	using System.Collections;

	using MediaTaster.Model;

	class TableBuilderNGUI : MonoBehaviour
	{
		public TextAsset m_configFile;
		private MediaTasterData m_data = null;

		public UILabel m_rowHeaderPrefab;
		public ColumnHeaderVisualisationNGUI m_columnVisualisationPrefab;
		public RowVisualisationNGUI m_rowVisualisationPrefab;
		public CellVisualisationNGUI m_cellVisualisationPrefab;
		
		public Transform m_rowHeaderParent;
		public Transform m_rowParent;
		public Transform m_columnTitleParent;

		private Table m_table;
		private List<UILabel> m_rowHeaders = new List<UILabel>();
		private List<RowVisualisationNGUI> m_rowVisuals = new List<RowVisualisationNGUI>();

		private Vector2 m_contentStart;

		void Awake()
		{
			// this creates ad initializes the config
			m_data = new MediaTasterData(m_configFile.bytes);
			string name = "movies";
			m_data.SetTable(name);
			m_table = m_data.GetTable(name);
		}

		void Start()
		{
			BuildColumnHeaders();

			CreateTableAndRowHeaderLayers(
				m_rowParent.transform.position
				, 0
				, m_table.Properties.RowsInOnePage);

			RefreshRows();
		}

		private Vector3 BuildColumnHeaders()
		{
			float x = m_columnTitleParent.transform.position.x;
			float y = m_columnTitleParent.transform.position.y;

			// go through all associated column labels and make gui for those
			foreach (xsd_ct_Column column in m_table.Columns)
			{
				// we do not make gui for element if its marked non-visible
				if (!column.IsVisible)
				{
					continue;
				}
				// make visualization
				ColumnHeaderVisualisationNGUI vis = (ColumnHeaderVisualisationNGUI)Instantiate(m_columnVisualisationPrefab);
				UITools.AssignAndPreserveTransformation(vis.transform, m_columnTitleParent);
				vis.Initialize(new Vector3(x, y, 0f), column);
			
				// update this column value
				column.X = x;
	
				// TODO: EI OIKEIN!
				Vector2 printedSize = 0.2f * vis.PrintSize;
				const float NodeRowOffsetX = 0.0f;
				x +=  column.Width + NodeRowOffsetX;
				y = Math.Max(y, printedSize.y);

				y = 0.0f;

			}
			return new Vector2(x, y);
		}

		private void CreateTableAndRowHeaderLayers(Vector3 startPoint, int startIndex, int count)
		{
			float x = startPoint.x;
			float y = startPoint.y;
			// make gui for all data rows
			for (int index = startIndex; index < count; index++)
			{
				// make a number header for the row
				UILabel rowHeader = (UILabel) Instantiate(m_rowHeaderPrefab);
				
				rowHeader.text = (index + 1).ToString();
				UITools.AssignAndPreserveTransformation(rowHeader.transform, m_rowHeaderParent);
				rowHeader.transform.localPosition = new Vector3(x, y, 0f);
				m_rowHeaders.Add(rowHeader);
			
				// make visualization
				RowVisualisationNGUI vis = (RowVisualisationNGUI)Instantiate(m_rowVisualisationPrefab);
				vis.Initialize(new Vector3(x, y, 0f), m_table.Columns, m_cellVisualisationPrefab);

				UITools.AssignAndPreserveTransformation(vis.transform, m_rowParent);

				y -= vis.Height;

				m_rowVisuals.Add(vis);
			}
		}

		private RowVisualisationNGUI GetNextVisualization(int index, bool makeMoreVisualizationsIfNeeded)
		{
			if (index >= m_rowVisuals.Count)
			{
				if (makeMoreVisualizationsIfNeeded)
				{
					float y = m_rowVisuals[m_rowVisuals.Count - 1].transform.position.y + m_rowVisuals[m_rowVisuals.Count - 1].Height;
					CreateTableAndRowHeaderLayers(new Vector2(m_contentStart.x, y), m_rowVisuals.Count, m_rowVisuals.Count * 2);
				}
				else
				{
					return null;
				}
			}
			return m_rowVisuals[index];
		}

		private Dictionary<TableRowEntry, int> m_rowToVisMap = new Dictionary<TableRowEntry, int>();

		public void RefreshRows()
		{
			// don't do nothing if not yet constructed
			if (m_rowVisuals.Count == 0)
				return;

			// make gui for all data rows
			int minIndex = m_table.PageIndex * m_table.Properties.RowsInOnePage;
			int maxIndex = minIndex + m_table.Properties.RowsInOnePage - 1;
			int realMaxIndex = Math.Min(maxIndex, m_table.FilteredRows.Count - 1);

			bool makeMoreVisualizationsIfNeeded = false;

			int visIndex = 0;
			RowVisualisationNGUI vis = GetNextVisualization(visIndex, makeMoreVisualizationsIfNeeded);

			float y = m_contentStart.y;

			// We want to show always as many choices in page as defined with RowsInOnePage,
			// regardless of the flag ShowOnlyAvailable (that might hide otherwise "valid" entries)
			// -> the indices ("index" -> this is shown first in the row) are still bound to actual item 
			// (e.g. not necessarily a running number, if ShowOnlyAvailable==true)
			for (int index = minIndex; visIndex < m_table.Properties.RowsInOnePage; index++)
			{
				TableRowEntry item = null;
				if (index < m_table.FilteredRows.Count)
				{
					item = m_table.FilteredRows[index];
				}
			
				vis = GetNextVisualization(visIndex, makeMoreVisualizationsIfNeeded);

				if (item != null && vis != null)
				{
					m_rowToVisMap.Add(item, visIndex);
				}
				if (vis == null)
				{
					break;
				}
				else
				{
					vis.Refresh(item, y);
					RefreshRowHeader(vis, item, index, visIndex, ref y);
					visIndex++;
				}
			}

			vis = GetNextVisualization(visIndex, false);
			while (vis != null)
			{
				vis.Refresh(null, y);
				RefreshRowHeader(vis, null, -1, visIndex, ref y);
				visIndex++;
				vis = GetNextVisualization(visIndex, false);
			}
		}

		private void RefreshRowHeader(RowVisualisationNGUI vis, TableRowEntry item, int index, int visIndex, ref float y)
		{
			UILabel rowHeader = m_rowHeaders[visIndex];
			if (item != null && index > -1)
			{
				Vector3 op = rowHeader.transform.position;
				rowHeader.transform.localPosition = new Vector3(op.x, y, op.z);
				rowHeader.text = (index + 1).ToString();

				NGUITools.SetActive(rowHeader.gameObject, true);
			}
			else
			{
				NGUITools.SetActive(rowHeader.gameObject, false);
			}
			if (vis != null)
			{
				y -= vis.Height;
			}
		}

	}
}