﻿
namespace MediaTaster.View
{
	using System;
	using UnityEngine;

	public static class UITools
	{
		public static void AssignAndPreserveTransformation(Transform transform, Transform parent)
		{
			if (parent == null)
				return;

			Vector3 scale = transform.localScale;
			Vector3 position = transform.position;
			Quaternion quat = transform.rotation;
			
			transform.parent = parent;
			
			transform.localScale = scale;
			transform.position = position;
			transform.rotation = quat;
		}
	}
}
