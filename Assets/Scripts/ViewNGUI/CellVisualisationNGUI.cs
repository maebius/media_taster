﻿
namespace MediaTaster.View
{
	using System;
	using UnityEngine;

	using MediaTaster.Model;

	class CellVisualisationNGUI : MonoBehaviour 
	{
		private TableCellEntry m_entry;

		private RowVisualisationNGUI m_row;

		private float m_height;

		private float m_cellTextOffset;

		public UILabel m_text;

		private xsd_ct_Column m_column;

		public void Initialize(xsd_ct_Column column)
		{
			m_column = column;
		}

		public float Height
		{
			get { return m_height; }
		}

		public void Refresh(RowVisualisationNGUI row, TableCellEntry entry)
		{
			// set data
			m_row = row;

			if (m_row != null) UITools.AssignAndPreserveTransformation(transform, row.transform);
	
			/*
			if (m_data != null)
			{
				m_data.Changed -= m_cellChangedHandler;
			}*/
			m_entry = entry;
			/*
			if (m_data != null)
			{
				m_data.Changed += m_cellChangedHandler;
			}*/
			RefreshView(m_row != null && m_entry != null);
		}

		private void RefreshView(bool enable)
		{
			NGUITools.SetActive(m_text.gameObject, enable);
		
			if (m_entry == null)
			{
				m_height = 0.0f;
				return;
			}

			float additionalOffsetX = 0.0f;
			string valueAsString = m_entry.ToPrettyString();

			/*
			if (m_column.PruneLeadingCategories)
			{
				int lastIndex = valueAsString.LastIndexOf("|");
				if (lastIndex > -1)
				{
					int count = valueAsString.Split('|').Length - 1;
					additionalOffsetX += count * 30.0f;
					valueAsString = valueAsString.Substring(lastIndex + 1).Trim();
				}
			}*/

			// init text object
			m_text.text = valueAsString;

			Vector3 op = Vector3.zero;
			transform.localPosition = new Vector3(
				op.x + m_column.X + m_cellTextOffset + additionalOffsetX
				, op.y + m_cellTextOffset
				, op.z);

			// TODO: EI OIKEIN!
			//m_height = 0.1f * m_text.relativeSize.y;
			m_height = 30f;

			/*
			if (m_column != null)
			{
				// are we too wide?
				float limit = m_column.Width - 2 * m_cellTextOffset;
				if (m_text.text.Length > limit)
				{
					const string POST_FIX = "...";

					float dotWidth = MainForm.Instance.GetStringSize(
						POST_FIX, m_text.Font).Width;
					valueAsString = MainForm.Instance.GetStringCroppedWithWidth(
						valueAsString, limit - dotWidth, m_text.Font) + POST_FIX;
				}
				// crop text
				m_text.Text = valueAsString;
				m_text.Width = limit;
			}
			m_height = m_text.Height + 2 * m_cellTextOffset;
			*/
		}
	}
}
