﻿namespace MediaTaster.View
{
	using System;
	using System.IO;
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.UI;
    using MediaTaster.Model;

    public class TableBuilder : MonoBehaviour
    {
		public RectTransform TableNamePanel;

        public TextAsset ConfigFile;
        public ColumnHeaderVisualisation ColumnVisualisationPrefab;
        public RowVisualisation RowVisualisationPrefab;
        public CellVisualisation CellVisualisationPrefab;
		public Text TableEntryPrefab;

		public TextAsset[] DatabaseFiles;

        public Canvas Canvas;

        public Text TableTitle;

        
        public void Awake()
        {
			Debug.Log(string.Format("persistentDataPath:{0}", Application.persistentDataPath));

			// copy our resources to location where we can actually write them as well, if not already existing
			if (DatabaseFiles != null)
			{
				foreach (var databaseFile in DatabaseFiles)
				{
					string writePath = string.Format("{0}/{1}", Application.persistentDataPath
					                                 , databaseFile.name);

					if (!File.Exists(writePath))
					{
						Debug.Log(string.Format("writing to:{0}", writePath));
						File.WriteAllBytes(writePath, databaseFile.bytes);
					}
				}
			}

            // this creates ad initializes the config
			m_data = new MediaTasterData(ConfigFile.bytes, Application.persistentDataPath);
            
        }

		public void Start()
		{
			m_contentStart = new Vector2(-Canvas.transform.position.x, TableTitle.rectTransform.localPosition.y - 50f);

			PopulateTablePanel();
		}

		private void PopulateTable(string name)
		{
			m_data.SetTable(name);
			m_table = m_data.GetTable(name);    

			if (m_table == null)
			{
				Debug.LogWarning(string.Format("Table is null!"));
				return;
			}

			TableTitle.text = m_table.Properties.Name;
			TableTitle.transform.localScale = 0.5f * TableTitle.transform.localScale;

			BuildColumnHeaders();
			RefreshRows();
		}

		private void PopulateTablePanel()
		{
			var offset = new Vector2(25f, 25f);

			var currentPos = new Vector2(offset.x, TableNamePanel.sizeDelta.y - offset.y);

			foreach (var entry in m_data.TableNames)
			{
				string name = entry;

				Text vis = (Text)Instantiate(TableEntryPrefab);
				vis.text = name;
				vis.transform.SetParent(TableNamePanel, true);

				Button button = vis.GetComponent<Button>();
				button.onClick = new Button.ButtonClickedEvent();
				button.onClick.AddListener(() => OnTableEntryClicked(name));

				currentPos = new Vector2(currentPos.x, currentPos.y - vis.rectTransform.sizeDelta.y);

				vis.rectTransform.localPosition = currentPos;
			}
		}

		private void OnTableEntryClicked(string name)
		{
			PopulateTable(name);
		}

        private void BuildColumnHeaders()
        {
            float x = m_contentStart.x;
            float y = m_contentStart.y;

			float relativeX = 0f;

			m_columnHeight = 0f;

			// destroy previous visualisations
			foreach (var v in m_columnHeaderVisualisations)
			{
				v.transform.SetParent(null);
				DestroyObject(v);
			}
			m_columnHeaderVisualisations.Clear();

            // go through all associated column labels and make gui for those
            foreach (xsd_ct_Column column in m_table.Columns)
            {
                // we do not make gui for element if its marked non-visible
                if (!column.IsVisible)
                {
                    continue;
                }
                // make visualization
                ColumnHeaderVisualisation vis = (ColumnHeaderVisualisation)Instantiate(ColumnVisualisationPrefab);
				m_columnHeaderVisualisations.Add (vis.gameObject);

			    vis.Initialize(Canvas.gameObject.transform, new Vector3(x, y, 0f), column);
				
                // update this column value
				column.X = relativeX;

				//Debug.Log(string.Format("TableBuilder.BuildColumnHeaders() - column:{0},x:{1}", column.PrintName, x));

				m_columnHeight = Math.Max(m_columnHeight, vis.RectTransform.sizeDelta.y);

                x += column.Width;
				relativeX += column.Width;
            }
        }

        private void CreateTableAndRowHeaderLayers(int startIndex, int count)
		{
			float x = m_contentStart.x;
			float y = m_contentStart.y - m_columnHeight;

			if (startIndex > 0)
			{
				var prevVisual = m_rowVisuals[startIndex - 1];
				y = prevVisual.RectTransform.localPosition.y - prevVisual.RectTransform.sizeDelta.y;
			}

			// make gui for all data rows
			for (int index = startIndex; index < count; index++)
			{
				//Debug.Log(string.Format("TableBuilder.CreateTableAndRowHeaderLayers() - index:{0},y:{1}", index, y));

				// make visualization
				RowVisualisation vis = (RowVisualisation)Instantiate(RowVisualisationPrefab);
				vis.Initialize(index, Canvas.gameObject.transform
					, new Vector3(x, y, 0f)
					, CellVisualisationPrefab);
				
				y -= vis.Height;

				m_rowVisuals.Add(vis);
			}
		}

		private RowVisualisation GetNextVisualization(int index, bool makeMoreVisualizationsIfNeeded)
		{
			if (index >= m_rowVisuals.Count)
			{
				if (makeMoreVisualizationsIfNeeded)
				{
					CreateTableAndRowHeaderLayers(m_rowVisuals.Count, (m_rowVisuals.Count + 1) * 2);
				}
				else
				{
					return null;
				}
			}
			return m_rowVisuals[index];
		}

		public void RefreshRows()
		{
			// make gui for all data rows
			int minIndex = m_table.PageIndex * m_table.Properties.RowsInOnePage;
			int maxIndex = minIndex + m_table.Properties.RowsInOnePage - 1;
			int realMaxIndex = Math.Min(maxIndex, m_table.FilteredRows.Count - 1);

			int visIndex = 0;
			RowVisualisation vis = GetNextVisualization(visIndex, true);

			float y = m_contentStart.y - m_columnHeight;

			// We want to show always as many choices in page as defined with RowsInOnePage,
			// regardless of the flag ShowOnlyAvailable (that might hide otherwise "valid" entries)
			// -> the indices ("index" -> this is shown first in the row) are still bound to actual item 
			// (e.g. not necessarily a running number, if ShowOnlyAvailable==true)
			for (int index = minIndex; visIndex < m_table.Properties.RowsInOnePage; index++)
			{
				TableRowEntry item = null;
				if (index < m_table.FilteredRows.Count)
				{
					item = m_table.FilteredRows[index];
				}
			
				vis = GetNextVisualization(visIndex, true);
				
				if (vis == null)
				{
					break;
				}
				else
				{
					vis.Refresh(m_table.Columns, index, item, y);
					y -= vis.Height;
					visIndex++;
				}
			}

			vis = GetNextVisualization(visIndex, false);
			while (vis != null)
			{
				vis.Refresh(null, -1, null, y);
				y -= vis.Height;
				visIndex++;

				vis = GetNextVisualization(visIndex, false);
			}
		}

		public void OnButtonRowsDown()
		{
			if (m_table == null)
				return;

			m_table.PageIndex++;
			RefreshRows();
		}

		public void OnButtonRowsUp()
		{
			if (m_table == null)
				return;

			m_table.PageIndex--;
			RefreshRows();
		}

		private List<GameObject> m_columnHeaderVisualisations = new List<GameObject>();

		private MediaTasterData m_data = null;

		private Table m_table;

		private List<RowVisualisation> m_rowVisuals = new List<RowVisualisation>();

		private Vector2 m_contentStart;
		private float m_columnHeight;
	}
}