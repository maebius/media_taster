﻿namespace MediaTaster.View
{
    using UnityEngine;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine.UI;
    using MediaTaster.Model;

    public class RowVisualisation : MonoBehaviour
    {
		public RectTransform RectTransform;
		public Text RowHeaderPrefab;
		
        public float Height { get; private set; }
        
		public void Awake()
		{
			RectTransform = GetComponent<RectTransform>();
		}

        /// <summary>
        /// All the visualisation objects of this rows elements.
        /// </summary>
      
        public void Initialize(int index, Transform parent, Vector3 pos
		                       , CellVisualisation cellPrefab)
        {
			//Debug.Log(string.Format("RowVisualisation.Initialize() - pos:{0}", pos.ToString()));
			RectTransform.SetParent(parent, true);
			RectTransform.localPosition = pos;
			RectTransform.SetAsFirstSibling();

			m_cellPrefab = cellPrefab;

			// make a number header for the row
			m_header = (Text)Instantiate(RowHeaderPrefab);
			m_header.rectTransform.SetParent(RectTransform, false);


        }

		private CellVisualisation GetVisualisation(int index)
		{
			if (index < m_cellVisPool.Count)
				return m_cellVisPool[index];

			CellVisualisation cell = (CellVisualisation)Instantiate(m_cellPrefab);
			m_cellVisPool.Add(cell);
			return cell;
		}

		private void RefreshRowHeader(int index, float y)
		{
			if (m_row != null && index > -1)
			{
				//Vector3 op = m_header.rectTransform.localPosition;
				//m_header.rectTransform.localPosition = new Vector3(op.x, y, op.z);
				m_header.text = (index + 1).ToString();
			}
		}

		public void Refresh(ICollection<xsd_ct_Column> columns, int index, TableRowEntry data, float y)
        {
            float maxHeight = 0;
            m_row = data;
		
			m_cellMap.Clear ();

			int visIndex = 0;
			if (columns != null)
			{
				// make visualisation for rows every actual visible element
				foreach (xsd_ct_Column column in columns)
				{
					if (!column.IsVisible)
						continue;
					CellVisualisation cell = GetVisualisation(visIndex++);
					cell.Initialize(RectTransform, column);
					m_cellMap[column] = cell;
				}
			}
			for (int i = 0; i < m_cellVisPool.Count; i++)
			{
				m_cellVisPool[i].enabled = false;
			}

            Vector3 pos = RectTransform.localPosition;
			RectTransform.localPosition = new Vector3(pos.x, y, pos.z);

			RefreshRowHeader(index, y);

			//Debug.Log(string.Format("RowVisualisation.Refresh() - pos:{0}", RectTransform.localPosition.ToString()));

			foreach (KeyValuePair<xsd_ct_Column, CellVisualisation> pair in m_cellMap)
            {
                xsd_ct_Column column = pair.Key;
                CellVisualisation vis = pair.Value;

                TableCellEntry cell = null;
                if (m_row != null)
                {
                    cell = m_row.Get(column.Index);
                }

                vis.Refresh(this, m_header.rectTransform.sizeDelta.x, cell);
                maxHeight = Math.Max(maxHeight, vis.Height);
            }

            Height = maxHeight;
        }

		private CellVisualisation m_cellPrefab;

		private List<CellVisualisation> m_cellVisPool = new List<CellVisualisation>();
		private Dictionary<xsd_ct_Column, CellVisualisation> m_cellMap =
			new Dictionary<xsd_ct_Column, CellVisualisation>();

		private Text m_header;
		private TableRowEntry m_row;
    }
}
