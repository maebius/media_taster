﻿namespace MediaTaster.View
{
    using UnityEngine;
    using System.Collections;
    using UnityEngine.UI;
    using MediaTaster.Model;

    public class ColumnHeaderVisualisation : MonoBehaviour
    {
        public Text Label;
		public RectTransform RectTransform;

        public Vector2 PrintSize
        {
            get { return m_printSize; }
        }

        private xsd_ct_Column m_column;
        private Vector2 m_printSize;

		public void Awake()
		{
			RectTransform = GetComponent<RectTransform>();
		}

        public void Initialize(Transform parent, Vector3 pos, xsd_ct_Column column)
        {
			RectTransform.SetParent(parent, false);
            Label.rectTransform.localPosition = pos;
			RectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, column.Width);

            m_column = column;
            Label.text = column.PrintName;
            // update for next column header element
            //m_printSize = Label.font.CalculatePrintedSize(m_label.text, true, UIFont.SymbolStyle.None);
        }
    }
}