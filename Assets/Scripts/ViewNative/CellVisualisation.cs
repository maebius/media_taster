﻿namespace MediaTaster.View
{
    using System;
    using UnityEngine;
    using UnityEngine.UI;
    using MediaTaster.Model;

    public class CellVisualisation : MonoBehaviour
    {
        public Text m_text;
		public RectTransform RectTransform;
       
		public void Awake()
		{
			RectTransform = GetComponent<RectTransform>();
		}

        public void Initialize(Transform parent, xsd_ct_Column column)
        {
			RectTransform.SetParent(parent, false);
            m_column = column;
        }

		public float Height { get; private set; }

        public void Refresh(RowVisualisation row, float xOffset, TableCellEntry entry)
        {
            // set data
            m_row = row;

            /*
            if (m_data != null)
            {
                m_data.Changed -= m_cellChangedHandler;
            }*/
            m_entry = entry;
			m_cellTextOffset = xOffset;
            /*
            if (m_data != null)
            {
                m_data.Changed += m_cellChangedHandler;
            }*/
            RefreshView(m_row != null && m_entry != null);
        }

        private void RefreshView(bool enable)
        {
			enabled = enable;
			//m_text.enabled = enable;
            if (m_entry == null)
            {
                Height = 0.0f;
                return;
            }

            string valueAsString = m_entry.ToPrettyString();

            /*
            if (m_column.PruneLeadingCategories)
            {
                int lastIndex = valueAsString.LastIndexOf("|");
                if (lastIndex > -1)
                {
                    int count = valueAsString.Split('|').Length - 1;
                    additionalOffsetX += count * 30.0f;
                    valueAsString = valueAsString.Substring(lastIndex + 1).Trim();
                }
            }*/

            // init text object
            m_text.text = valueAsString;
			Vector3 newPos = new Vector3(
                m_column.X + m_cellTextOffset
                , 0f
                , 0f);

			//Debug.Log(string.Format("CellVisualisation.RefreshView() - text:{0},pos:{1}", valueAsString, newPos));

			RectTransform.localPosition = newPos;
			RectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, m_column.Width);

            Height = RectTransform.sizeDelta.y;

            /*
            if (m_column != null)
            {
                // are we too wide?
                float limit = m_column.Width - 2 * m_cellTextOffset;
                if (m_text.text.Length > limit)
                {
                    const string POST_FIX = "...";

                    float dotWidth = MainForm.Instance.GetStringSize(
                        POST_FIX, m_text.Font).Width;
                    valueAsString = MainForm.Instance.GetStringCroppedWithWidth(
                        valueAsString, limit - dotWidth, m_text.Font) + POST_FIX;
                }
                // crop text
                m_text.Text = valueAsString;
                m_text.Width = limit;
            }
            m_height = m_text.Height + 2 * m_cellTextOffset;
            */
        }


		public void OnButton()
		{
			Debug.Log(string.Format("I was clicked:{0}", m_entry.ToString()));

		}


		private TableCellEntry m_entry;
		private RowVisualisation m_row;
		private float m_cellTextOffset;
		private xsd_ct_Column m_column;
    }
}
