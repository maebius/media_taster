<?xml version="1.0"?>
<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema"
			targetNamespace="http://www.maeland.net"
			xmlns="http://www.maeland.net"
			elementFormDefault="qualified">

	<xsd:simpleType name="xsd_st_IP">
		<xsd:restriction base="xsd:string">
			<xsd:pattern value="((([1-9]?[0-9])|(1[0-9][0-9])|(2[0-4][0-9])|(25[0-5]))\.){3}(([1-9]?[0-9])|(1[0-9][0-9])|(2[0-4][0-9])|(25[0-5]))">
				<xsd:annotation>
					<xsd:documentation>
						s
						IP address, on range [0-255].[0-255].[0-255].[0-255], such as 127.0.0.1
					</xsd:documentation>
				</xsd:annotation>
			</xsd:pattern>
		</xsd:restriction>
	</xsd:simpleType>

	<xsd:simpleType name="xsd_stringWithSpaces">
		<xsd:annotation>
			<xsd:documentation>
				String with all the spaces preserved (usually xml interpreters take these away).
			</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="xsd:string">
			<xsd:whiteSpace value="preserve"/>
		</xsd:restriction>
	</xsd:simpleType>

	<xsd:simpleType name="xsd_entryType">
		<xsd:annotation>
			<xsd:documentation>
				Defines what is to be printed on table cell. If "value", then straight the cell value,
				if "searchCount", then the count for search result (this requires TableSearch value element).
			</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="xsd:string">
			<xsd:enumeration value="constant"/>
			<xsd:enumeration value="calculation"/>
			<xsd:enumeration value="tableValue"/>
			<xsd:enumeration value="combination"/>
			<xsd:enumeration value="mapped"/>
			<xsd:enumeration value="tableValueWithCondition"/>
			<xsd:enumeration value="tableValueWithInverseCondition"/>
			<xsd:enumeration value="bookmark"/>
			<xsd:enumeration value="sqlQuery"/>
		</xsd:restriction>
	</xsd:simpleType>

	<xsd:simpleType name="xsd_iterationType">
		<xsd:annotation>
			<xsd:documentation>
			</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="xsd:string">
			<xsd:enumeration value="Count"/>
			<xsd:enumeration value="Average"/>
			<xsd:enumeration value="Min"/>
			<xsd:enumeration value="Max"/>
		</xsd:restriction>
	</xsd:simpleType>

	<xsd:complexType name="xsd_ct_Iteration">
		<xsd:annotation>
			<xsd:documentation>
			</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element name="TableSearch"  type="xsd_ct_TableSearch" minOccurs="1" maxOccurs="unbounded"/>
		</xsd:sequence>
		<xsd:attribute name="Type" type="xsd_iterationType" use="required"/>
		<xsd:attribute name="Column" type="xsd:string" use="optional"/>
	</xsd:complexType>

	<xsd:complexType name="xsd_ct_DataSource">
		<xsd:attribute name="Id" type="xsd:string"/>
	</xsd:complexType>

	<xsd:complexType name="xsd_ct_Database">
		<xsd:annotation>
			<xsd:documentation>
				Datatype defining a mysql server, with connection information as well.
			</xsd:documentation>
		</xsd:annotation>
		<xsd:complexContent>
			<xsd:extension base="xsd_ct_DataSource">
				<xsd:sequence>
					<xsd:element name="Server" type="xsd_st_IP" minOccurs="1" maxOccurs="1"/>
					<xsd:element name="Port" type="xsd:nonNegativeInteger" minOccurs="1" maxOccurs="1"/>
					<xsd:element name="Userid" type="xsd:string" minOccurs="1" maxOccurs="1"/>
					<xsd:element name="Password" type="xsd:string" minOccurs="1" maxOccurs="1"/>
					<xsd:element name="Name" type="xsd:string" minOccurs="1" maxOccurs="1"/>
				</xsd:sequence>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>

	<xsd:complexType name="xsd_ct_File">
		<xsd:annotation>
			<xsd:documentation>
				Datatype defining a file in local disk.
			</xsd:documentation>
		</xsd:annotation>
		<xsd:complexContent>
			<xsd:extension base="xsd_ct_DataSource">
				<xsd:sequence>
					<xsd:element name="Filename" type="xsd:string" minOccurs="1" maxOccurs="1"/>
					<xsd:element name="Name" type="xsd:string" minOccurs="1" maxOccurs="1"/>
				</xsd:sequence>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>

	<xsd:complexType name="xsd_ct_ColumnLink">
		<xsd:annotation>
			<xsd:documentation>
				Defines a link for column, that is followed if the entry in table is clicked.
			</xsd:documentation>
		</xsd:annotation>
	</xsd:complexType>

	<xsd:complexType name="xsd_ct_TableTarget">
		<xsd:annotation>
			<xsd:documentation>
				Defines parameters for a table entry, namely a table and column. This can be used
				as a source or target, so that a pair of typeSearch instances define a mapping from
				source to target. Can be extended to include also database, or substrings inside entry, etc.
			</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element name="Table"  type="xsd:string" minOccurs="1" maxOccurs="1"/>
			<xsd:element name="Column" type="xsd:string" minOccurs="1" maxOccurs="1"/>
			<xsd:element name="Row" type="xsd:int" minOccurs="0" maxOccurs="1"/>
		</xsd:sequence>
	</xsd:complexType>

	<xsd:complexType name="xsd_ct_Restriction">
		<xsd:annotation>
			<xsd:documentation>
			</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element name="ColumnId" type="xsd:string" minOccurs="1" maxOccurs="unbounded"/>
		</xsd:sequence>
		<xsd:attribute name="Duplicates" type="xsd:boolean" use="optional" default="false"/>
	</xsd:complexType>

	<xsd:complexType name="xsd_ct_TableSearch">
		<xsd:annotation>
			<xsd:documentation>
				A mapping from source table value to target table value.
			</xsd:documentation>
		</xsd:annotation>
		<xsd:complexContent>
			<xsd:extension base="xsd_ct_ColumnLink">
				<xsd:sequence>
					<xsd:element name="Source" type="xsd_ct_TableTarget" minOccurs="1" maxOccurs="1"/>
					<xsd:element name="Target" type="xsd_ct_TableTarget" minOccurs="1" maxOccurs="1"/>
				</xsd:sequence>
				<xsd:attribute name="Duplicates" type="xsd:boolean" use="optional" default="true"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>

	<xsd:complexType name="xsd_ct_Mapping">
		<xsd:annotation>
			<xsd:documentation>
				Defines a mapping from one string to another. Used to replace all occurences
				of source with target in a search.
			</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element name="Source" type="xsd_stringWithSpaces" minOccurs="1" maxOccurs="1"/>
			<xsd:element name="Target" type="xsd_stringWithSpaces" minOccurs="1" maxOccurs="1"/>
		</xsd:sequence>
	</xsd:complexType>

	<xsd:simpleType name="xsd_ParameterType">
		<xsd:annotation>
			<xsd:documentation>
			</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="xsd:string">
			<xsd:enumeration value="Constant"/>
			<xsd:enumeration value="TableValue"/>
			<xsd:enumeration value="RuntimeMapped"/>
			<xsd:enumeration value="BookmarkValue"/>
		</xsd:restriction>
	</xsd:simpleType>

	<xsd:complexType name="xsd_ct_Parameter">
		<xsd:annotation>
			<xsd:documentation>
			</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element name="Value" type="xsd:string" minOccurs="0" maxOccurs="1"/>
			<xsd:element name="Replace" type="xsd_ct_Mapping" minOccurs="0" maxOccurs="unbounded"/>
		</xsd:sequence>
		<xsd:attribute name="Type" type="xsd_ParameterType" use="required"/>
	</xsd:complexType>

	<xsd:complexType name="xsd_ct_Combination">
		<xsd:annotation>
			<xsd:documentation>
			</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element name="Parameter" type="xsd_ct_Parameter" minOccurs="1" maxOccurs="unbounded"/>
		</xsd:sequence>
		<xsd:attribute name="Name" type="xsd:string" use="required"/>
	</xsd:complexType>

	<xsd:complexType name="xsd_ct_TargetPruning">
		<xsd:annotation>
			<xsd:documentation>
			</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element name="RequiredValue" type="xsd:string" minOccurs="1" maxOccurs="unbounded"/>
			<xsd:element name="Parameter" type="xsd_ct_Parameter" minOccurs="1" maxOccurs="unbounded"/>
		</xsd:sequence>
	</xsd:complexType>

	<xsd:complexType name="xsd_ct_RegistryProcess">
		<xsd:annotation>
			<xsd:documentation>
				Defines a call to process using registry entry.
			</xsd:documentation>
		</xsd:annotation>
		<xsd:complexContent>
			<xsd:extension base="xsd_ct_ColumnLink">
				<xsd:sequence>
					<xsd:element name="TargetPruning" type="xsd_ct_TargetPruning" minOccurs="0" maxOccurs="unbounded"/>
					<xsd:element name="Parameter" type="xsd_ct_Parameter" minOccurs="1" maxOccurs="unbounded"/>
				</xsd:sequence>
				<xsd:attribute name="Key" type="xsd:string" use="required"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>

	<xsd:complexType name="xsd_ct_Color">
		<xsd:annotation>
			<xsd:documentation>
				Defines a color mapping.
			</xsd:documentation>
		</xsd:annotation>
		<xsd:complexContent>
			<xsd:extension base="xsd_ct_ColumnLink">
				<xsd:sequence>
					<xsd:element name="Condition" type="xsd_ct_Mapping" minOccurs="0" maxOccurs="unbounded"/>
					<xsd:element name="ConditionInverse" type="xsd_ct_Mapping" minOccurs="0" maxOccurs="unbounded"/>
				</xsd:sequence>
				<xsd:attribute name="Value" type="xsd:string" use="required"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>

	<xsd:complexType name="xsd_ct_BookmarkTarget">
		<xsd:annotation>
			<xsd:documentation>
				Defines a bookmark mapping for column.
			</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element name="RegistryProcess" type="xsd_ct_RegistryProcess" minOccurs="1" maxOccurs="1"/>
		</xsd:sequence>
		<xsd:attribute name="Name" type="xsd:string" use="required"/>
	</xsd:complexType>

	<xsd:complexType name="xsd_ct_Icon">
		<xsd:annotation>
			<xsd:documentation>
			</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element name="TargetPruning" type="xsd_ct_TargetPruning" minOccurs="0" maxOccurs="unbounded"/>
		</xsd:sequence>
		<xsd:attribute name="FromFile" type="xsd:string" use="optional"/>
		<xsd:attribute name="FromRegistry" type="xsd:string" use="optional"/>
		<xsd:attribute name="Default" type="xsd:boolean" use="optional" default="false"/>
	</xsd:complexType>


	<xsd:complexType name="xsd_ct_Column">
		<xsd:annotation>
			<xsd:documentation>
				Datatype defining a tables column. The columns have certain properties,
				as if it's visible, does it link to some other table/column, etc.
			</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element name="Icon" type="xsd_ct_Icon" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element name="Combination" type="xsd_ct_Combination" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element name="RegistryProcess" type="xsd_ct_RegistryProcess" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element name="TableSearch" type="xsd_ct_TableSearch" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element name="Iteration" type="xsd_ct_Iteration" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element name="Condition" type="xsd_ct_Mapping" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element name="ConditionInverse" type="xsd_ct_Mapping" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element name="DefaultValue" type="xsd:string" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element name="ColorBg" type="xsd_ct_Color" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element name="ColorText" type="xsd_ct_Color" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element name="ColorSelectBg" type="xsd_ct_Color" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element name="ColorSelectText" type="xsd_ct_Color" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element name="BookmarkTarget" type="xsd_ct_BookmarkTarget" minOccurs="0" maxOccurs="unbounded"/>
		</xsd:sequence>
		<xsd:attribute name="Name" type="xsd:string" use="required"/>
		<xsd:attribute name="PrintName" type="xsd:string" use="required"/>
		<xsd:attribute name="PrintNameInSelection" type="xsd:string" use="optional"/>
		<xsd:attribute name="ValueModifiedDataColumnName" type="xsd:string" use="optional"/>
		<xsd:attribute name="IsVisible" type="xsd:boolean" use="optional" default="true"/>
		<xsd:attribute name="IsVisibleInSelection" type="xsd:boolean" use="optional" default="true"/>
		<xsd:attribute name="Editable" type="xsd:boolean" use="optional" default="false"/>
		<xsd:attribute name="Deletable" type="xsd:boolean" use="optional" default="false"/>
		<xsd:attribute name="ListOnlyWhenAdding" type="xsd:boolean" use="optional" default="false"/>
		<xsd:attribute name="EntryType" type="xsd_entryType" use="optional" default="tableValue"/>
		<xsd:attribute name="Width" type="xsd:int" use="optional"/>
		<xsd:attribute name="IndexEnabled" type="xsd:boolean" use="optional" default="false"/>
		<xsd:attribute name="IsEnum" type="xsd:boolean" use="optional"/>
		<xsd:attribute name="PrintIfEmpty" type="xsd:string" use="optional" default="-"/>
		<xsd:attribute name="LinkEnabled" type="xsd:boolean" use="optional" default="true"/>
		<xsd:attribute name="MultiValueWhenAdding" type="xsd:boolean" use="optional" default="false"/>
		<xsd:attribute name="TextScale" type="xsd:float" use="optional" default="1.0"/>
		<xsd:attribute name="FormatAsFileSize" type="xsd:boolean" use="optional" default="false"/>
		<xsd:attribute name="PruneLeadingCategories" type="xsd:boolean" use="optional" default="true"/>
		<xsd:attribute name="SourceQuery" type="xsd:string" use="optional" default=""/>
		<xsd:attribute name="QueryIndexColumn" type="xsd:string" use="optional" default=""/>
	</xsd:complexType>

	<xsd:complexType name="xsd_ct_Argument">
		<xsd:annotation>
			<xsd:documentation>
			</xsd:documentation>
		</xsd:annotation>
		<xsd:attribute name="Name" type="xsd:string" use="required"/>
		<xsd:attribute name="Value" type="xsd:string" use="optional"/>
	</xsd:complexType>

	<xsd:element name="HeaderElement" type="xsd_HeaderElement"/>
	<xsd:complexType name="xsd_HeaderElement">
		<xsd:annotation>
			<xsd:documentation>
			</xsd:documentation>
		</xsd:annotation>
		<xsd:attribute name="Name" type="xsd:string" use="optional" default=""/>
		<xsd:attribute name="Id" type="xsd:string" use="optional" default=""/>
		<xsd:attribute name="Editable" type="xsd:boolean" use="optional" default="false"/>
		<xsd:attribute name="TextScale" type="xsd:float" use="optional" default="1.0"/>
		<xsd:attribute name="ColorFG" type="xsd:string" use="optional" default="Black"/>
		<xsd:attribute name="ColorBG" type="xsd:string" use="optional" default="White"/>
	</xsd:complexType>

	<xsd:element name="HeaderCommandLine" type="xsd_ct_CommandLine" substitutionGroup="HeaderElement"/>
	<xsd:complexType name="xsd_ct_CommandLine">
		<xsd:annotation>
			<xsd:documentation>
			</xsd:documentation>
		</xsd:annotation>
		<xsd:complexContent>
			<xsd:extension base="xsd_HeaderElement">
				<xsd:sequence>
					<xsd:element name="Argument" type="xsd_ct_Argument" minOccurs="0" maxOccurs="unbounded"/>
				</xsd:sequence>
				<xsd:attribute name="Executable" type="xsd:string" use="required"/>
				<xsd:attribute name="RunPath" type="xsd:string" use="optional"/>
				<xsd:attribute name="UpdateAfter" type="xsd:boolean" use="optional" default="false"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>

	<xsd:element name="HeaderPython" type="xsd_ct_PythonCommand" substitutionGroup="HeaderElement"/>
	<xsd:complexType name="xsd_ct_PythonCommand">
		<xsd:annotation>
			<xsd:documentation>
			</xsd:documentation>
		</xsd:annotation>
		<xsd:complexContent>
			<xsd:extension base="xsd_HeaderElement">
				<xsd:sequence>
					<xsd:element name="Argument" type="xsd_ct_Argument" minOccurs="0" maxOccurs="unbounded"/>
				</xsd:sequence>
				<xsd:attribute name="File" type="xsd:string" use="required"/>
				<xsd:attribute name="UpdateAfter" type="xsd:boolean" use="optional" default="false"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>

	<xsd:element name="HeaderSearch" type="xsd_ct_HeaderSearch" substitutionGroup="HeaderElement"/>
	<xsd:complexType name="xsd_ct_HeaderSearch">
		<xsd:annotation>
			<xsd:documentation>
			</xsd:documentation>
		</xsd:annotation>
		<xsd:complexContent>
			<xsd:extension base="xsd_HeaderElement">
				<xsd:sequence>
					<xsd:element name="Search" type="xsd_ct_TableSearch" minOccurs="1" maxOccurs="1"/>
					<xsd:element name="ColumnName" type="xsd:string" minOccurs="1" maxOccurs="unbounded"/>
				</xsd:sequence>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>

	<xsd:complexType name="xsd_ct_Header">
		<xsd:annotation>
			<xsd:documentation>
			</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element ref="HeaderElement" minOccurs="0" maxOccurs="unbounded"/>
		</xsd:sequence>
		<xsd:attribute name="Topic" type="xsd:string" use="optional"/>
	</xsd:complexType>

	<xsd:complexType name="xsd_ct_SearchLink">
		<xsd:annotation>
			<xsd:documentation>
			</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element name="Mapping" type="xsd_ct_Mapping" minOccurs="0" maxOccurs="unbounded"/>
		</xsd:sequence>
		<xsd:attribute name="PrintName" type="xsd:string" use="required"/>
		<xsd:attribute name="TableName" type="xsd:string" use="required"/>
		<xsd:attribute name="PartialMatch" type="xsd:boolean" use="optional" default="false"/>
		<xsd:attribute name="IsDefault" type="xsd:boolean" use="optional" default="false"/>
	</xsd:complexType>

	<xsd:complexType name="xsd_ct_Query">
		<xsd:annotation>
			<xsd:documentation>
				Represents a named query, where the query is just a string of MySQL query language and executed as it is.
			</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element name="SQL" type="xsd:string" minOccurs="1" maxOccurs="1"/>
		</xsd:sequence>
		<xsd:attribute name="Name" type="xsd:string" use="required"/>
	</xsd:complexType>

	<xsd:complexType name="xsd_ct_ActivityFeed">
		<xsd:annotation>
			<xsd:documentation>
				Collection for gathering instances of Activity - entities.
			</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element name="Query" type="xsd_ct_Query" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element name="Table" type="xsd_ct_Table" minOccurs="0" maxOccurs="unbounded"/>
		</xsd:sequence>
		<xsd:attribute name="Name" type="xsd:string" use="required"/>
		<xsd:attribute name="Database" type="xsd:string" use="required"/>
	</xsd:complexType>

	<xsd:complexType name="xsd_ct_Table">
		<xsd:annotation>
			<xsd:documentation>
				Datatype defining a table with columns. The columns have certain properties,
				as if it's visible, does it link to some other table/column, etc.
			</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element name="SourceId" type="xsd:string" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element name="SourceDirectory" type="xsd:string" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element name="UpdatedTable" type="xsd:string" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element name="Header" type="xsd_ct_Header" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element name="SearchLink" type="xsd_ct_SearchLink" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element name="Restriction" type="xsd_ct_Restriction" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element name="Column" type="xsd_ct_Column" minOccurs="0" maxOccurs="unbounded"/>
		</xsd:sequence>
		<xsd:attribute name="Name" type="xsd:string" use="required"/>
		<xsd:attribute name="PrintName" type="xsd:string" use="optional"/>
		<xsd:attribute name="PrintNameScale" type="xsd:float" use="optional" default="1.0"/>
		<xsd:attribute name="Source" type="xsd:string" use="required"/>
		<xsd:attribute name="Listed" type="xsd:boolean" use="optional" default="true"/>
		<xsd:attribute name="ColumnToGroup" type="xsd:string" use="optional"/>
		<xsd:attribute name="DefaultColumnForIndex" type="xsd:string" use="required"/>
		<xsd:attribute name="DefaultColumnToSort" type="xsd:string" use="optional"/>
		<xsd:attribute name="DefaultColumnToSortIncreasing" type="xsd:boolean" use="optional" default="true"/>
		<xsd:attribute name="RowsInOnePage" type="xsd:int" default="100"/>
		<xsd:attribute name="Image" type="xsd:string" use="optional"/>
		<xsd:attribute name="DoInitialLoading" type="xsd:boolean" use="optional" default="true"/>
		<xsd:attribute name="InitializeDatabase" type="xsd:boolean" use="optional" default="true"/>
		<xsd:attribute name="InfoText" type="xsd:boolean" use="optional" default="true"/>
	</xsd:complexType>

	<xsd:element name="xsd_MediaTaster">
		<xsd:complexType>
			<xsd:sequence>
				<xsd:element name="DatabaseSource" type="xsd_ct_Database" minOccurs="0" maxOccurs="unbounded"/>
				<xsd:element name="FileSource" type="xsd_ct_File" minOccurs="0" maxOccurs="unbounded"/>
				<xsd:element name="HarddiscMapping" type="xsd_ct_Mapping" minOccurs="0" maxOccurs="unbounded"/>
				<xsd:element name="ActivityFeed" type="xsd_ct_ActivityFeed" minOccurs="0" maxOccurs="unbounded"/>
				<xsd:element name="Table" type="xsd_ct_Table" minOccurs="0" maxOccurs="unbounded"/>
			</xsd:sequence>
			<xsd:attribute name="ColorFG" type="xsd:string" use="optional" default="Black"/>
			<xsd:attribute name="ColorBG" type="xsd:string" use="optional" default="White"/>
		</xsd:complexType>
	</xsd:element>

</xsd:schema>